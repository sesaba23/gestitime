/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ssb.gestitime.utils;

import java.util.Calendar;

/**
 * gestitime
 * Created by sergio on 13/05/16.
 *
 * Tools to convert seconds in format time (hours, minutes and seconds)
 */
public class TimeUtility {

    public static String convertSecondsInTime(long seconds){

        long hours = seconds / 3600;
        long minutes = (seconds % 3600) / 60;
        long secs = seconds % 60;
        return String.format("%02d:%02d:%02d", hours, minutes, secs);
    }
    public static String convertSecondsInTimeWithoutColons(long seconds){

        long hours = seconds / 3600;
        long minutes = (seconds % 3600) / 60;
        long secs = seconds % 60;
        return String.format("%02d%02d%02d", hours, minutes, secs);
    }

    public static long[] splitSeconds(long seconds){
        long[] result = new long[3];

        result[0] = seconds / 3600; //Calculate Hours
        result[1] = (seconds % 3600) / 60; //Calculate Minutes
        result[2] = seconds % 60; //Calculate Seconds

        return result;
    }

    public static String getCurrentTime(){
        Calendar calendar = Calendar.getInstance();
        //Show the start time of the countdown
        int cHour = calendar.get(Calendar.HOUR_OF_DAY); //Get hour in 24h format
        int cMinute = calendar.get(Calendar.MINUTE);
        int cSecond = calendar.get(Calendar.SECOND);
        return String.format("%02d", cHour)+
                ":"+String.format("%02d", cMinute)+":"+String.format("%02d", cSecond);

    }
}
