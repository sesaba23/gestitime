/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ssb.gestitime.utils;

import com.ssb.gestitime.BuildConfig;

/**
 * getitime
 * Created by sergio on 6/05/16.
 *
 * Provide a log that not appear in RELEASE versions
 */
public class MyLogger {

    static final boolean LOG = BuildConfig.DEBUG;

    public static void d(String tag, String string){
        if(LOG) android.util.Log.d(tag, string);
    }
}
