/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ssb.gestitime.utils;

/**
 * GestiTime Created by Sergio Sánchez Barahona on 27/07/16.
 */
public class Constants {

    public interface ACTION{
        String SECONDS = "com.ssb.gestitime.action.seconds";
        String ACTION = "com.ssb.gestitime.action.action";
        String ACTUAL_TIME = "com.ssb.gestitime.action.actualtime";
        String PLAY_ACTION = "com.ssb.gestitime.action.play";
        String PAUSE_ACTION = "com.ssb.gestitime.action.pause";
        String STOP_ACTION = "com.ssb.gestitime.action.stop";
        String NO_ACTION = "com.ssb.gestitime.action.no";
    }
}
