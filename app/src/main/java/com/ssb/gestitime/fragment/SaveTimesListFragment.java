/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ssb.gestitime.fragment;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.app.ListFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.CursorAdapter;

import android.support.v4.widget.DrawerLayout;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.SimpleCursorSwipeAdapter;
import com.ssb.gestitime.R;
import com.ssb.gestitime.adapters.MySimpleCursorSwypeAdapter;
import com.ssb.gestitime.databases.CgestitimeDataBaseHelper;
import com.ssb.gestitime.databases.Ctime;
import com.ssb.gestitime.databases.DatabaseContract;
import com.ssb.gestitime.utils.MyLogger;
import com.ssb.gestitime.utils.TimeUtility;

import java.util.ArrayList;


/**
 * gestitime
 * 15/06/2016. Sergio Sánchez Barahona. sesaba23@gmail.com
 * A simple {@link Fragment} subclass.
 */
public class SaveTimesListFragment extends ListFragment {

    private SQLiteDatabase db;
    private Context mContext;

    private ArrayList<Ctime> CtimesArray; //Stores times which are going to be shown in the listview

    private OnRecordSelectedListener mCallback;
    //Container Activity must implement this interface
    public interface OnRecordSelectedListener{
        void onRecordSelected(ArrayList<Ctime> CtimesArray, String record);
    }

    public SaveTimesListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*This makes sure that the container activity has implemented the callback interface.
        * If not, it throws an exception*/
        try{
            mCallback = (OnRecordSelectedListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement OnRecordSelectedListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        try{
            mCallback = (OnRecordSelectedListener) mContext;
        }catch (ClassCastException e){
            throw new ClassCastException(mContext.toString() + " must implement OnRecordSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.mContext = inflater.getContext();
        CtimesArray = new ArrayList<>();

        View view = super.onCreateView(inflater, container, savedInstanceState);

        /* Get a reference of the dataBase. If it doesn't exits create it*/
        CgestitimeDataBaseHelper gt = new CgestitimeDataBaseHelper(mContext);
        db = gt.getWritableDatabase();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String[] projection = new String[]{DatabaseContract.RecordTimers._ID,
                DatabaseContract.RecordTimers.NAME_RECORD,
                DatabaseContract.RecordTimers.DESCRIPTION_RECORD};

        final Cursor cursor = db.query(DatabaseContract.RecordTimers.TABLE_NAME, //Table
                projection, //Projection
                null,       //Selection
                null,       //Selection Args
                null, null, null);  //groupBy, having and orderBy

        final MySimpleCursorSwypeAdapter listAdapter = new MySimpleCursorSwypeAdapter(mContext,
                R.layout.record_list_fragment_row, //layout
                cursor,                              //Cursor
                new String[]{DatabaseContract.RecordTimers.NAME_RECORD, DatabaseContract.RecordTimers.DESCRIPTION_RECORD}, //Display Columns
                new int[]{R.id.name_record, R.id.description_record},  //text Views of the layout where which views you want to display them in
                0);

        setListAdapter(listAdapter);

        /**
         *  Set dynamically the height of the FrameLayout that contains the listFragment
         * */
//Example to get pixels into dp
//        DisplayMetrics metrics = new DisplayMetrics();
//        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        float logicalDensity = metrics.density;
//        int dp = (int) Math.ceil(getTotalHeightofListView(listAdapter) * logicalDensity);

//        FrameLayout frameLayout = (FrameLayout)getActivity().findViewById(R.id.save_records_listview);
//        ViewGroup.LayoutParams params = frameLayout.getLayoutParams();
//        params.height = getTotalHeightofListView(listAdapter);
//        frameLayout.setLayoutParams(params);

        /**
         * Set onClickListener in the listView in order to select the item clicked and load in timers listView
         */
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cursor.moveToPosition(position);
                String recordSelected = cursor.getString(cursor.getColumnIndex(DatabaseContract.RecordTimers.NAME_RECORD));
                String ID = cursor.getString(cursor.getColumnIndex(DatabaseContract.RecordTimers._ID));

                //Select times of the record selected from LIST_RECORD_TIMERS table
                String[] projection = new String[]{DatabaseContract.ListRecordTimers._ID,
                                                    DatabaseContract.ListRecordTimers.ID_RECORD,
                                                    DatabaseContract.ListRecordTimers.TIME_ITEM};
                final Cursor cursorList = db.query(DatabaseContract.ListRecordTimers.TABLE_NAME, //Table
                        projection, //Projection
                        DatabaseContract.ListRecordTimers.ID_RECORD + " = ?",       //Selection
                        new String[]{ID},       //Selection Args
                        null, null, DatabaseContract.ListRecordTimers._ID);  //groupBy, having and orderBy

                CtimesArray.clear();
                cursorList.moveToFirst();
                do{
                    Ctime time = new Ctime(cursorList.getLong(2), cursorList.getLong(2), TimeUtility.convertSecondsInTime(cursorList.getLong(2)));
                    CtimesArray.add(time);
                    MyLogger.d("SaveTimesFragment", "Seconds add to the array: " + cursorList.getLong(2));
                }while (cursorList.moveToNext());
                cursorList.close();

                //Send the event to the host activity
                mCallback.onRecordSelected(CtimesArray, recordSelected);

                DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

    }

    /**
     * Get total heigh of the listFragment
     * @param mAdapter
     * @return
     */
    private int getTotalHeightofListView(MySimpleCursorSwypeAdapter mAdapter) {
        int listViewElementsHeight = 0;
        int extraHeight = 0;
        for (int i = 0; i < mAdapter.getCount(); i++) {
            View mView = mAdapter.getView(i, null, getListView());
            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            listViewElementsHeight += mView.getMeasuredHeight();
            extraHeight = mView.getMeasuredHeight();
        }
        //Add extra space not to cut the last record
        listViewElementsHeight = listViewElementsHeight + extraHeight + 45;
        //Add height of all dividers of the listView
        listViewElementsHeight = listViewElementsHeight + (getListView().getDividerHeight() * (mAdapter.getCount() - 1));
        return listViewElementsHeight;
    }
}
