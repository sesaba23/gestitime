/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ssb.gestitime.fragment;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.github.aakira.expandablelayout.ExpandableLayoutListener;
import com.github.aakira.expandablelayout.ExpandableWeightLayout;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;
import com.ssb.gestitime.ManageActionsNotificationService;
import com.ssb.gestitime.R;
import com.ssb.gestitime.TimerService;
import com.ssb.gestitime.adapters.MyRecyclerArrayAdapter;
import com.ssb.gestitime.databases.CgestitimeDataBaseHelper;
import com.ssb.gestitime.databases.Ctime;
import com.ssb.gestitime.databases.DatabaseContract;
import com.ssb.gestitime.pickers.CustomNumberPicker;
import com.ssb.gestitime.utils.Constants;
import com.ssb.gestitime.utils.DeviceType;
import com.ssb.gestitime.utils.MyLogger;
import com.ssb.gestitime.utils.TimeUtility;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * gestitime
 * 26/05/2016. Sergio Sánchez Barahona. sesaba23@gmail.com
 *
 * An {@link Fragment} subclass for control and show UI layout to implement gestitime functions.
 *
 * On top of the UI there is a FrameLayout that shows, either number pickers in order to let user select
 * time, or the countdown when start button is pressed.
 * In the middle there is listview showing times introduced by the user and a number picker where one
 * notifications period can easily be selected
 * At the bottom there are buttons that allow user start, pause, stop, add times or reset UI.
 * These button are shown depending of the information show in the UI.
 *
 * Implements a TimeService.CallBack to communicate with the countdown service in order to update
 * countdown view when user clicks start button.
 *
 */
public class MainFragment extends Fragment implements TimerService.CallBacks, ManageActionsNotificationService.CallBacks
                    ,View.OnClickListener, NumberPicker.OnValueChangeListener{

    private View view; //Reference to the fragment layout view
    private Menu menu; //Reference to action bar icons
    private Toolbar toolbar;

    private long seconds = 0;
    private int numNotifications = 0;

    private ArrayList<Ctime> CtimesArray; //Stores times which are going to be shown in the listview
    private int position; //Helps to know which time has been passed in the view

    //Reference to layouts and views
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private ScrollView showTime;
    private LinearLayout timeSetPicker;
    private LinearLayout periodSetPicker;
    private LinearLayout periodBottomWrapper;
    private ExpandableWeightLayout listviewLayout;
    private RecyclerView recyclerView;
    private TextView vButtonStart;
    private TextView vButtonPause;
    private TextView vButtonStop;
    private TextView vButtonAdd;
    private NumberPicker vHoursPicker;
    private NumberPicker vMinutesPicker;
    private NumberPicker vSecondsPicker;
    private NumberPicker vPeriodPicker;
    //Custom timer Picker to show countdown
    private CustomNumberPicker customCountDownView;
    private TextView countDownView;
    private TextView countView;
    private TextView vPeriodText;
    private TextView vPeriodHelpText;
    private MenuItem vFullScreenItem;
    private SwipeLayout swipeLayout;
    /* Views that show information about notifications configuration*/
    private ImageView allNotificationsAreSet;
    private ImageView finishSoundIsSet;
    private ImageView notificationSoundIsSet;
    private ImageView finishVibrationIsSet;
    private ImageView notificationVibrationIsSet;
    private TextView timeVibrationIsSet;
    private ImageView ledIsSet;
    private ImageView screenHoldOnIsSet;
    private TextView ledText1;
    private TextView ledText2;
    private TextView ledText3;
    /* Icons drawables that show information about notifications configuration */
    private Drawable notificationsON;
    private Drawable notificationsOFF;
    private Drawable finishSoundIcon;
    private Drawable notificationSoundIcon;
    private Drawable finishVibrationIcon;
    private Drawable notificationVibrationIcon;
    private Drawable ledIcon;
    private Drawable screenHoldOnIcon;
    private FloatingActionButton fab;

    private float defaultPositionY;
    private boolean flagListViewAnimation;
    private boolean flagFloatingActionButtonShow;
    private boolean flagToolTipSwipe;
    /* This flag replace view.isShow() because it is only true when the activity is in the foreground */
    private boolean flagCountdownView;
    /* Helps to know if countdown is paused or running*/
    private boolean flagCountdownPause;

    private MyRecyclerArrayAdapter recyclerAdapter;

    private final Handler handler = new Handler(); //control blink countdown text when it is paused

    /* Variables to manage/connect to the app services implementing countdown and notifications action buttons*/
    private Intent timerIntent; //References to an timerIntent so whe can launch the service
    private Intent notificationIntent;
    private TimerService timer; //Reference to function timer
    private ManageActionsNotificationService notificationService;
    private ServiceConnection timerConnection, notificationConnection;

    //Helps to delete recyclerView rows when swipe on it
    private ItemTouchHelper itemTouchHelper;

    /**
     * Constructor of the class.
     * Initialize variables. It also connects and binds fragment with the countdown service and
     * notification action buttons service
     */
    public MainFragment() {
        CtimesArray = new ArrayList<>();
        position = 0;
        /* Initialize flags*/
        flagListViewAnimation = false;
        flagFloatingActionButtonShow = true; //When we start app we can show fab if user set some time
        flagToolTipSwipe = true;
        flagCountdownView = false;
        flagCountdownPause = false;

         /*Initialize service timerConnection*/
        timerConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                TimerService.TimerBinder timerBinder = (TimerService.TimerBinder) service;
                timer = timerBinder.getTimerService();
                timer.registerClient(MainFragment.this); //Set callback in this class to update seconds
                MyLogger.d("HowServicesWork", "MainFragment Connected to timer service has been completed");
            }
            @Override
            //Not called with unbind Method, only when the system loses the connection
            public void onServiceDisconnected(ComponentName name){
                MyLogger.d("HowServicesWork", "MainFragment has been disconnected to timer service");
                timer.stopTimer(Constants.ACTION.STOP_ACTION);
            }
        };

        /* Initialize service notificationConnection*/
        notificationConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                /* Connect to the manageActionsNotification Service*/
                ManageActionsNotificationService.NotificationBinder notificationBinder = (ManageActionsNotificationService.NotificationBinder) service;
                notificationService = notificationBinder.getTimerService();
                notificationService.registerClient(MainFragment.this); //Set callback in this class to manage countdown from notification buttons
            }
            @Override
            public void onServiceDisconnected(ComponentName name){ }
        };
    }

    public long getSeconds() {
        return seconds;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {

        /* If we come back from a configuration change (rotate), load data from previous configuration*/
        if(savedInstanceState != null){
            CtimesArray = savedInstanceState.getParcelableArrayList("ARRAY");
        }

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_main, container, false);
        MyLogger.d("ChangeConfig", "Creating view of new fragment: " + getClass().getName());
        MyLogger.d("ChangeConfig", "Number of timers: " + CtimesArray.size());

        //Enable Menu in fragment so we can access action bar icons
        setHasOptionsMenu(true);

        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        //Fist time don't show the keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        /* Start Services */
        timerIntent = new Intent(getActivity(), TimerService.class);
        notificationIntent = new Intent(getActivity(), ManageActionsNotificationService.class);
        getActivity().bindService(timerIntent, timerConnection, Context.BIND_AUTO_CREATE); //Creates the Timer service if it doesn't already exist
        getActivity().bindService(notificationIntent, notificationConnection, Context.BIND_AUTO_CREATE); //Creates the  Notification service if it doesn't already exist

        //Set layouts and views
        initLayoutsAndViews();
        setInitView();

        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create an instance of the dialog fragment and show it
                DialogFragment dialog = new RecordTimersDialog();
                dialog.show(getFragmentManager(), "RecordTimersDialog");

//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        readAndShowNotificationsPreferences();
        if(CtimesArray.size() != 0) {
            flagListViewAnimation = true;
            setListView();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unbindService(timerConnection);
        //We need to stopService Manually because the service class implements onStartCommand Method
        getActivity().stopService(timerIntent);
        MyLogger.d("HowServicesWork", "MainFragment Destroyed");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("ARRAY", CtimesArray);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        this.menu = menu;
        vFullScreenItem = menu.findItem(R.id.action_full_screen);
        vFullScreenItem.setVisible(false);
        showToolTips(vButtonAdd);

    }

    /**
     * CallBack with TimeService to show the time
     * @param seconds
     */
    @Override
    public void updateClient(long seconds, int numNotifications) {
        // Stores the actual time in case we paused the countdown
        this.seconds = seconds;
        this.numNotifications = numNotifications;
        if(seconds >= 0) {

//            if(DeviceType.isTablet(getActivity())){
//
//                customCountDownView.changeValueByOne(false);
//
//            }
            //Show countdown
            String time = TimeUtility.convertSecondsInTime(seconds);
            countDownView.setText(time);
            //Show time passed since countdown started

            MyLogger.d("ChangeConfig", "MainFragment CTimes Array size: " + CtimesArray.size());
            countView.setText(TimeUtility.convertSecondsInTime(CtimesArray.get(CtimesArray.size()-1).getTimeSeconds() - seconds));
            MyLogger.d("watchSecondsHandler", "CountDown is: " + time);
            /* If current time equals some time saved in the list view... */
            if((CtimesArray.get(CtimesArray.size()-1).getTimeSeconds() - seconds) == CtimesArray.get(position).getTimeSeconds()){
                /* ...  Mark as passed */
                CtimesArray.get(position).setCompleted(true);
                setListView();
                //Mark the next position of the listview as target
                position++;
            }
            /* Update time left till the notification in each row of the recyclerView */
            long auxSeconds = (CtimesArray.get(CtimesArray.size()-1).getTimeSeconds()) - seconds;
            for(int i = 0; i < CtimesArray.size(); i++) {
                long actualTimeLeft = CtimesArray.get(i).getTimeSeconds() - auxSeconds;
                if(CtimesArray.get(i).getTimeLeft() > 0) {
                    CtimesArray.get(i).setTimeLeft(actualTimeLeft);
                }
            }
            setListView();

            //When countdown finishes enable buttons and stop service
            if(seconds == 0){
                //Show only the countdown
                setStopView();
                timerIntent.putExtra(TimerService.NUM_NOTIFICATIONS, 0);
                getActivity().stopService(timerIntent);
            }
        }
    }

    /**
     * Start, if it is possible, the countdown
     * @return boolean   If it is possible to start the countdown return true
     */
    public boolean startClicked(){
        // If the countdown has not started yet, start the countdown from the beginning
        if(timeSetPicker.isShown()) {
            for(int j = 0; j<CtimesArray.size(); j++){
                CtimesArray.get(j).setCompleted(false);
                CtimesArray.get(j).setTimeLeft(CtimesArray.get(j).getTimeSeconds());
                setListView();
            }
            position = 0;
            //The user only wants a simple countdown
            if(CtimesArray.size() == 0){
                deleteOrAddTimes(readTimeFieldsInformation(view), 0, false);
                //If the time equals zero don't anything
                if(CtimesArray.get(0).getTimeSeconds() == 0){
                    prepareStartCounting();
                    CtimesArray.clear();
                    return false;
                }
            }
            //Pass total time and listview to the service
            timerIntent.putExtra(TimerService.SECONDS, CtimesArray.get(CtimesArray.size() - 1).getTimeSeconds());
            timerIntent.putParcelableArrayListExtra(TimerService.ARRAY, CtimesArray);

            ((TextView)view.findViewById(R.id.started_time_view)).setText(TimeUtility.getCurrentTime());
            timerIntent.putExtra(Constants.ACTION.ACTUAL_TIME, TimeUtility.getCurrentTime());
        }else{ // If the countdown is paused start countdown from the stored time
            timerIntent.putExtra(TimerService.SECONDS, this.seconds);
            timerIntent.putExtra(TimerService.NUM_NOTIFICATIONS, numNotifications);
            timerIntent.putParcelableArrayListExtra(TimerService.ARRAY, CtimesArray);
            handler.removeCallbacksAndMessages(null); //use null when we don't want to reference runnable
            countDownView.clearAnimation(); //Show countdown immediately
            countView.clearAnimation();
        }
        return true;
    }

    /**
     * Pauses the countDown
     */
    public void pauseClicked(){
        timer.stopTimer(Constants.ACTION.PAUSE_ACTION);
        blinkView();
    }

    /**
     * Stops the countdown and restart the time picker with the last time introduced
     */
    public void stopClicked(){
       //If user has just introduced a time but has not clicked start or add yet and wants to reset timerpicker...
       if(CtimesArray.size() == 0){
           vHoursPicker.setValue(0);
           vMinutesPicker.setValue(0);
           vSecondsPicker.setValue(0);
           vButtonStop.setVisibility(View.GONE);
           vPeriodPicker.setEnabled(false);
           vPeriodPicker.setMaxValue(99);
           toolbar.setTitle(getResources().getString(R.string.app_name));
           vPeriodHelpText.setText(getString(R.string.period_help_text_no_time_selected));

       }else {
           /* We only need to stop countdown if the countdown has not started yet. If STOP button text is RESET
            * countdown has no started. We can not use "timer.isTimerStarted()" because countdown is not started if it is paused, and we need:
            *  If countdown has started...STOP countdown. If countdown is PAUSE and then we press STOP
            *  we need to call stopTimer in order to passed action Constant so notification was canceled*/
           if(!vButtonStop.getText().equals(getString(R.string.resume_button_text)) & !vButtonStop.getText().equals(getString(R.string.reset_button_text))) {
               timer.stopTimer(Constants.ACTION.STOP_ACTION);
           }

           getActivity().stopService(timerIntent);

           handler.removeCallbacksAndMessages(null); //use null when we don't want to reference runnable

           //Get time in seconds and split into hours, minutes and seconds
           long lastTimeInSeconds = CtimesArray.get(CtimesArray.size() - 1).getTimeSeconds();
           long splitedTime[] = TimeUtility.splitSeconds(lastTimeInSeconds);

           //If the time show in the time picker is different to last time or we press stop when the countdown is running... set the last time
           if(lastTimeInSeconds != (vHoursPicker.getValue()*3600 + vMinutesPicker.getValue()*60 + vSecondsPicker.getValue())
                   | flagCountdownView) {
               //Show the last countdown introduced when the countdown started
               vHoursPicker.setValue((int) splitedTime[0]);
               vMinutesPicker.setValue((int) splitedTime[1]);
               vSecondsPicker.setValue((int) splitedTime[2]);
               vPeriodPicker.setEnabled(true);
           }else{ //If time show in the time picker is same as last time... reset the view
               toolbar.setTitle(getResources().getString(R.string.app_name));
               vButtonStop.setVisibility(View.GONE);
               CtimesArray.clear();
               setListView();
               vHoursPicker.setValue(0);
               vMinutesPicker.setValue(0);
               vSecondsPicker.setValue(0);
               vPeriodPicker.setEnabled(false);
               vPeriodPicker.setValue(1);
               vPeriodPicker.setMaxValue(99);
               vPeriodHelpText.setText(getString(R.string.period_help_text_no_time_selected));
               //Hide fab if it is already show and reset flag so we can show fab again when time is set is deleteOrAddTimes()
               if(!flagFloatingActionButtonShow) {
                   YoYo.with(Techniques.SlideOutDown).delay(200).duration(1000).playOn(fab);
                   flagFloatingActionButtonShow = true;
               }
           }
       }
        //If periodPicker is open close it
        swipeLayout.close(true);
    }

    /**
     * When the ADD button is clicked add the time to the listview, unless it is in it already or time is zero.
     */
    public void addClicked(){
        //Get NumberPicker views
        long seconds = (vHoursPicker.getValue()*3600 + vMinutesPicker.getValue()*60 + vSecondsPicker.getValue());

        //Only add time to the listview if there users has introduce one time
        if(seconds != 0) {
            if(lookForDuplicates(seconds)) {
                vButtonStop.setVisibility(View.VISIBLE);
                deleteOrAddTimes(readTimeFieldsInformation(view), 0, false);
                //If the user add one time to the listview is not possible choosing a period
                periodSetPicker.setEnabled(true);
            }else{
                Toast.makeText(getActivity(), R.string.duplicate_time, Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(getActivity(), R.string.no_time_typed, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Look for the time pass in the array. If the time already exists return true
     */
    private boolean lookForDuplicates(long seconds){
        for(int i = 0; i < CtimesArray.size(); i++){
            if(CtimesArray.get(i).getTimeSeconds() == seconds){
                return false;
            }
        }
        return true;
    }

    /**
     * Starts the TimerService
     */
    private void prepareStartCounting(){
        seconds = CtimesArray.get(0).getTimeSeconds();
        if(seconds > 0) {
            if(!timer.isServiceStarted()) {
                getActivity().startService(timerIntent);
                MyLogger.d("ChangeConfig", "MainFragment after start service CTimes Array size: " + CtimesArray.size());
            }
        }else{
            Toast.makeText(getActivity(), R.string.no_time_typed, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Read the time introduced int the number pickers
     * @param view
     */
    private int readTimeFieldsInformation(View view){

        int hoursPicker = vHoursPicker.getValue();
        int minutesPicker = vMinutesPicker.getValue();
        int secondsPicker = vSecondsPicker.getValue();

        return hoursPicker * 3600 + minutesPicker * 60 + secondsPicker;
    }

    /**
     * Add a new time or delete the swipe row time of the recyclerView. If time is added sort chronologically
     * @param seconds the time we want to add to de recyclerView
     * @param position The position we want to delete
     * @param delete true if we want to delete or false if we want to add
     */
    private void deleteOrAddTimes(long seconds, int position, boolean delete){
        if(!delete) {
            Ctime time = new Ctime(seconds, seconds, TimeUtility.convertSecondsInTime(seconds));
            recyclerAdapter.addData(time);
        }else{
            recyclerAdapter.deleteData(position);
        }
        //Only show fab the first time this method is getting call if there is some time selected and fab is already show
        if(CtimesArray.size() > 0) {
            if (CtimesArray.get(0).getTimeSeconds() != 0 & flagFloatingActionButtonShow) {
                fab.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.BounceInUp).delay(0).duration(1000).playOn(fab);
                flagFloatingActionButtonShow = false;
            }
        }
        //Hide fab if it is not timers in recyclerView and reset flag so we can show fab again when time is set is deleteOrAddTimes()
        if(CtimesArray.size() == 0 & !flagFloatingActionButtonShow) {
            YoYo.with(Techniques.SlideOutDown).delay(200).duration(1000).playOn(fab);
            flagFloatingActionButtonShow = true;
        }
        showOrHideLisviewLayout();
    }

    /**
     * Reloads listiview to update
     */
    private void setListView(){
        //We only set adapter first time. Then only update Array so user can scroll listview when it is countdown
        if(recyclerView.getAdapter() == null) {
            recyclerAdapter = new MyRecyclerArrayAdapter(getActivity(), R.layout.recycler_row, CtimesArray);
            recyclerView.setAdapter(recyclerAdapter);
        }else {
            //updateData is a custom method defined in MyArrayAdapter and scroll listview to show next intervals
            recyclerAdapter.updateData(CtimesArray);

            View v = recyclerView.getChildAt(0);
            int bottom = (v == null) ? 0 : (v.getBottom() - recyclerView.getPaddingBottom());
//            recyclerView.smoothScrollToPosition(position);
            if(timeSetPicker.isShown()){
                recyclerView.scrollBy(0, bottom);//smoothScrollByOffset(bottom);
            }else {
                recyclerView.smoothScrollToPosition(position);
            }
        }
        showOrHideLisviewLayout();
    }

    /**
     *  This method manage when de recyclerView layout has to be shown or not.
     *  It is only show if there is some time to add to the recyclerView
     *  @return animate     true if the have had an animation in the timers layout. False if not*/
    private boolean showOrHideLisviewLayout(){
        if(!flagListViewAnimation & !CtimesArray.isEmpty()) {
            if(CtimesArray.get(0).getTimeSeconds() != 0) {
                listviewLayout.toggle();
                flagListViewAnimation = true;
                return true;
            }
        }
        else if(flagListViewAnimation & CtimesArray.isEmpty()){
            listviewLayout.toggle();
            flagListViewAnimation = false;
            return true;
        }
        return false;
    }

    /**
     * Provide a simple method to setListView from the MainActivity to load saved records show in the NavigationDrawer
     * @param CtimesArray
     */
    public void setCTimesArray(ArrayList<Ctime> CtimesArray){
        //Enable "RESET" button
        vButtonStop.setVisibility(View.VISIBLE);
        //Hide fab if it is already shown and reset flag so we can show fab again when time is set is deleteOrAddTimes()
        if(!flagFloatingActionButtonShow) {
            YoYo.with(Techniques.SlideOutDown).delay(200).duration(1000).playOn(fab);
            flagFloatingActionButtonShow = true;
        }

        this.CtimesArray.clear();
        for(int i = 0; i < CtimesArray.size(); i++){
            this.CtimesArray.add(CtimesArray.get(i)); //More efficient. It is not necessary to sort again times of the array
        }
        setListView();
    }

    /**
     * Provide a simple method to Save times set by user in the recyclerView into the DataBase
     */
    public void saveListviewInDatabase(String title, String description){
        CgestitimeDataBaseHelper gt = new CgestitimeDataBaseHelper(getActivity());
        SQLiteDatabase db = gt.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseContract.RecordTimers.NAME_RECORD, title);
        values.put(DatabaseContract.RecordTimers.DESCRIPTION_RECORD, description);
        values.put(DatabaseContract.RecordTimers.ID_USER, 0);

        long lastID = db.insert(DatabaseContract.RecordTimers.TABLE_NAME, null, values);

        values.clear();
        if(lastID != -1) {
            for (int i = 0; i < CtimesArray.size(); i++) {
                values.put(DatabaseContract.ListRecordTimers.ID_RECORD, lastID);
                values.put(DatabaseContract.ListRecordTimers.TIME_ITEM, CtimesArray.get(i).getTimeSeconds());
                db.insert(DatabaseContract.ListRecordTimers.TABLE_NAME, null, values);
                values.clear();
            }
        }else{
            Toast.makeText(getActivity(), getString(R.string.error_saving_record), Toast.LENGTH_LONG).show();
        }
        db.close();

        //Update navigation drawer in order to make appear the new record
        SaveTimesListFragment saveTimesListFragment = new SaveTimesListFragment();
        FragmentTransaction ft2 = getFragmentManager().beginTransaction();
        ft2.replace(R.id.save_records_listview, saveTimesListFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    /**
     * Set the period time configuring layout according the value of the numberPickers
     * @param picker
     * @param oldVal
     * @param newVal
     */
    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        int id = picker.getId();
        switch (id) {
            //If we change value in the period number picker
            case R.id.period_time_set_picker:
                long seconds = readTimeFieldsInformation(view);
                //Only calculate the times according to period if the user has introduce almost one time and period > 1
                if (seconds > 0 & newVal > 0) {
                    CtimesArray.clear();
                    deleteOrAddTimes(seconds, 0, false);
                    long aux = seconds / newVal;
                    for (int i = 0; i < (newVal - 1); i++) {
                        seconds = (CtimesArray.get(CtimesArray.size() - 1 - i).getTimeSeconds() - aux);
                        deleteOrAddTimes(seconds, 0, false);
                    }
                    setListView();
                }
                break;
            default: //If we change value in one of the times number picker
                //When almost one of the time number picker change...
                float actualTimeSelected = (float) readTimeFieldsInformation(view) / 60.0f; //Period minimum sensibility is one minute
                MyLogger.d("MainFragment", "Time in the number pickers: " + TimeUtility.convertSecondsInTime((int)actualTimeSelected));

                if(flagToolTipSwipe){
                    showToolTips(view.findViewById(R.id.minutes_text_picker));
                    flagToolTipSwipe = false;
                }

                //If the user introduced some time...
                if(actualTimeSelected > 0.0f){
                    vPeriodPicker.setEnabled(true);
                    vButtonStop.setVisibility(View.VISIBLE);
                    vButtonStop.setText(R.string.reset_button_text);
                    if(actualTimeSelected >= 1.0f){
                        vPeriodHelpText.setText(getString(R.string.period_help_text_select_period));
                        //Maximum number of period is 99
                        if(actualTimeSelected > 99){
                            vPeriodPicker.setMaxValue(99);
                        }else{
                            vPeriodPicker.setMaxValue((int)actualTimeSelected);
                        }
                    }else {
                        vPeriodHelpText.setText(getString(R.string.period_help_text_min_time));
                    }
                }else { //If the actual time is zero disable the view and delete STOP button
                    vPeriodPicker.setEnabled(false);
//                    vButtonStop.setVisibility(View.GONE);
                    vPeriodPicker.setValue(1);
                    vPeriodPicker.setMaxValue(99);
                    vPeriodHelpText.setText(getString(R.string.period_help_text_no_time_selected));
                }
                break;
        }
    }

    /**
     * Implements the logic button after setting listeners to the buttons
     * @param v
     */
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.button_start:
                //If it is possible to start countdown, start the countdown
                MyLogger.d("ChangeConfig", "Before Click START CTimes Array size: " + CtimesArray.size());
                if(startClicked()) {
                    flagCountdownPause = false;
                    //If we start from the beginning animate timer view. Service is started in animateCountdown Method
                    if(timeSetPicker.isShown()) {
                        animateCountDown();
                    }else{ //If we come back from a pause start the service again
                        prepareStartCounting();
                    }
                    setRunningCountdownView();
                    MyLogger.d("ChangeConfig", "After Click START CTimes Array size: " + CtimesArray.size());
                }
                break;
            case R.id.button_pause:
                flagCountdownPause = true;
                pauseClicked();
                setPauseCountdown();
                break;
            case R.id.button_stop:
                flagCountdownPause = false;
                stopClicked();
                setStopView();
                break;
            case R.id.button_add:
                addClicked();
                break;
        }
    }

    /**
     * Implement blinking countdown view when user press PAUSE
     */
    private void blinkView(){
        final int DURATION = 1200;
        final Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setDuration(DURATION);
        final Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setDuration(DURATION);

        handler.post(new Runnable() {
            boolean flag = false;
            @Override
            public void run() {
                if(!flag) {
                    countDownView.startAnimation(fadeOut);
                    countView.startAnimation(fadeOut);
                    flag = true;
                }else{
                    countDownView.startAnimation(fadeIn);
                    countView.startAnimation(fadeIn);
                    flag = false;
                }
                handler.postDelayed(this, DURATION);
            }
        });
    }

    private void initLayoutsAndViews(){
        /**
         * Get layout views and views
         */

        if(DeviceType.isTablet(getActivity())){

//            customCountDownView = (CustomNumberPicker) view.findViewById(R.id.custom_picker_time_view);
        }

        //Countdown view is only shown when start counting
        showTime = (ScrollView) view.findViewById(R.id.layout_time_view);
        timeSetPicker = (LinearLayout)view.findViewById(R.id.time_set_picker);
        periodSetPicker = (LinearLayout)view.findViewById(R.id.period_time_set_picker);
        periodBottomWrapper = (LinearLayout)view.findViewById(R.id.period_bottom_wrapper);
        listviewLayout = (ExpandableWeightLayout)view.findViewById(R.id.list_view_layout);
        recyclerView = (RecyclerView)view.findViewById(R.id.times_list_view);
        vButtonStart = (TextView)view.findViewById(R.id.button_start);
        vButtonPause = (TextView)view.findViewById(R.id.button_pause);
        vButtonStop = (TextView)view.findViewById(R.id.button_stop);
        vButtonAdd = (TextView)view.findViewById(R.id.button_add);
        vHoursPicker = (NumberPicker)view.findViewById(R.id.hours_time_set_picker);
        vMinutesPicker = (NumberPicker)view.findViewById(R.id.minutes_time_set_picker);
        vSecondsPicker = (NumberPicker)view.findViewById(R.id.seconds_time_set_picker);
        vPeriodPicker = (NumberPicker)view.findViewById(R.id.period_time_set_picker);
        vPeriodText = (TextView)view.findViewById(R.id.period_text);
        vPeriodHelpText = (TextView) view.findViewById(R.id.period_help_text);
        //Get reference to time_view to show seconds in the UI
        countDownView = (TextView) view.findViewById(R.id.time_view);
        countView = (TextView) view.findViewById(R.id.passed_time_view);
        //Get reference of drawerlayout and toggle
        drawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                getActivity(), drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        /**
         * Get references of Views that show information about notifications configuration
         */
        allNotificationsAreSet = (ImageView)view.findViewById(R.id.all_notifications_are_set);
        finishSoundIsSet = (ImageView)view.findViewById(R.id.finish_sound_is_set);
        notificationSoundIsSet = (ImageView)view.findViewById(R.id.notification_sound_is_set);
        finishVibrationIsSet = (ImageView)view.findViewById(R.id.finish_vibration_is_set);
        notificationVibrationIsSet = (ImageView)view.findViewById(R.id.notification_vibration_is_set);
        timeVibrationIsSet = (TextView)view.findViewById(R.id.time_vibration);
        screenHoldOnIsSet = (ImageView)view.findViewById(R.id.hold_screen_on_is_set);

        ledIsSet = (ImageView)view.findViewById(R.id.led_is_set);
        ledText1 = (TextView)view.findViewById(R.id.led_distribution_text_1);
        ledText2 = (TextView)view.findViewById(R.id.led_distribution_text_2);
        ledText3 = (TextView)view.findViewById(R.id.led_distribution_text_3);

        /**
         * Get references of icons that show information about notifications configuration
         */
        notificationsON = getActivity().getResources().getDrawable(R.mipmap.ic_volume_down_black_18dp);
        notificationsOFF = getActivity().getResources().getDrawable(R.mipmap.ic_do_not_disturb_on_18pt_3x);
        finishSoundIcon = getActivity().getResources().getDrawable(R.mipmap.ic_alarm_black_18dp);
        notificationSoundIcon = getActivity().getResources().getDrawable(R.mipmap.ic_add_alert_black_18dp);
        finishVibrationIcon = getActivity().getResources().getDrawable(R.mipmap.ic_settings_remote_black_18dp);
        notificationVibrationIcon = getActivity().getResources().getDrawable(R.mipmap.ic_vibration_black_18dp);
        ledIcon = getActivity().getResources().getDrawable(R.mipmap.ic_lightbulb_outline_black_18dp);
        screenHoldOnIcon = getActivity().getResources().getDrawable(R.mipmap.ic_dvr_black_18dp);


        /**
         * Configure swipe on layout timer in order to enable interval picker
         */
        swipeLayout =  (SwipeLayout)view.findViewById(R.id.set_timer_layout_swipe);
        //set show mode.
        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        //add drag edge.(If the BottomView has 'layout_gravity' attribute, this line is unnecessary)
        swipeLayout.addDrag(SwipeLayout.DragEdge.Right, view.findViewById(R.id.period_bottom_wrapper));

        defaultPositionY = timeSetPicker.getY();

        /**
         * Set Listeners
         */
        vButtonStart.setOnClickListener(this);
        vButtonPause.setOnClickListener(this);
        vButtonStop.setOnClickListener(this);
        vButtonAdd.setOnClickListener(this);
        vPeriodPicker.setOnValueChangedListener(this);
        vHoursPicker.setOnValueChangedListener(this);
        vMinutesPicker.setOnValueChangedListener(this);
        vSecondsPicker.setOnValueChangedListener(this);
        /* If user opens drawerLayout and periodSetPicker is open (swipe), close it*/
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) { }
            @Override
            public void onDrawerOpened(View drawerView) {
                swipeLayout.close(true);
            }
            @Override
            public void onDrawerClosed(View drawerView) { }
            @Override
            public void onDrawerStateChanged(int newState) { }
        });

        /**
         * Set and configure recyclerView
         */
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setListView();
        /**
         * Set swipe to delete row in recyclerView
         */
        itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(500);
        itemAnimator.setMoveDuration(500);
        recyclerView.setItemAnimator(itemAnimator);

    }


    /**
     * Set the init view when the application has just launch
     */
    private void setInitView(){
        //When app is launched the showtime layout must be hide
        showTime.setVisibility(View.INVISIBLE);
        vButtonPause.setVisibility(View.GONE);
        vButtonStop.setVisibility(View.GONE);
        vPeriodPicker.setEnabled(false);
    }

    /**
     * Read notifications preferences to show they state in the main layout
     */
    private void readAndShowNotificationsPreferences(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        //Set if sound and vibration notifications are enabled
        final boolean notificationsFlag = preferences.getBoolean("notifications_new_message", true);
        //Set finish countdown sound notification
        final String finishSoundFlag = preferences.getString("end_notifications_ringtone", "");
        //Set notifications sound. If user has choose "silent" in preferences, don't play any sound
        final String notificationSoundChoose = preferences.getString("notifications_ringtone", "");
        //Set finish vibration.
        final boolean finishVibrationFlag = preferences.getBoolean("finished_vibration", true);
        //Set notification vibration
        final boolean notificationVibrationFlag = preferences.getBoolean("notification_vibration", true);
        //Set the time vibration selected
        final String timeNotificationVibration = preferences.getString("time_notification_vibration", "300");
        //Set led notification and time percentage
        final boolean ledFlag = preferences.getBoolean("led_notification", true);
        final String notificationsColorLed = preferences.getString("led_time_notification", "0");
        //Set hold on screen selected option
        final boolean holdOnScreenFlag = preferences.getBoolean("hold_on_screen", false);

        if(notificationsFlag){//If notifications sounds and vibration are ON...
            notificationsON.mutate().setColorFilter(new PorterDuffColorFilter(0xFF1b5e20, PorterDuff.Mode.SRC_ATOP));
            allNotificationsAreSet.setImageDrawable(notificationsON);
            if(!finishSoundFlag.equals("")){ //If Finish sound is ON...
                finishSoundIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFF1b5e20, PorterDuff.Mode.SRC_ATOP));
                finishSoundIsSet.setImageDrawable(finishSoundIcon);
            }else{ //...If Finish sound is OFF
                finishSoundIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
                finishSoundIsSet.setImageDrawable(finishSoundIcon);
            }
            if(!notificationSoundChoose.equals("")){ //If notifications sounds are ON...
                notificationSoundIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFF1b5e20, PorterDuff.Mode.SRC_ATOP));
                notificationSoundIsSet.setImageDrawable(notificationSoundIcon);
            }else{ //...If notifications sounds are OFF
                notificationSoundIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
                notificationSoundIsSet.setImageDrawable(notificationSoundIcon);
            }
            if(finishVibrationFlag){ //If finish vibration is ON ...
                finishVibrationIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFF1b5e20, PorterDuff.Mode.SRC_ATOP));
                finishVibrationIsSet.setImageDrawable(finishVibrationIcon);

            }else{ //...If finish vibration is OFF
                finishVibrationIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
                finishVibrationIsSet.setImageDrawable(finishVibrationIcon);
            }
            if(notificationVibrationFlag){ // If notification vibration is ON...
                notificationVibrationIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFF1b5e20, PorterDuff.Mode.SRC_ATOP));
                notificationVibrationIsSet.setImageDrawable(notificationVibrationIcon);
                timeVibrationIsSet.setText(timeNotificationVibration + "ms");
                timeVibrationIsSet.setVisibility(View.VISIBLE);
            }else{ //...If notification vibration is OFF
                notificationVibrationIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
                notificationVibrationIsSet.setImageDrawable(notificationVibrationIcon);
                timeVibrationIsSet.setVisibility(View.GONE);
            }
        }else{ //...If notifications are OFF
            notificationsOFF.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
            allNotificationsAreSet.setImageDrawable(notificationsOFF);
            finishSoundIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
            finishSoundIsSet.setImageDrawable(finishSoundIcon);
            notificationSoundIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
            notificationSoundIsSet.setImageDrawable(notificationSoundIcon);
            finishVibrationIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
            finishVibrationIsSet.setImageDrawable(finishVibrationIcon);
            notificationVibrationIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
            notificationVibrationIsSet.setImageDrawable(notificationVibrationIcon);
            timeVibrationIsSet.setVisibility(View.GONE);
        }

        /* LEd notifications are in a separate menu and they don't depend of sounds and vibration notifications*/
        if(ledFlag){ //If LED notification is ON...
            ledIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFF1b5e20, PorterDuff.Mode.SRC_ATOP));
            ledIsSet.setImageDrawable(ledIcon);

            ledText1.setVisibility(View.VISIBLE);
            ledText2.setVisibility(View.VISIBLE);
            ledText3.setVisibility(View.VISIBLE);
            int selection = Integer.parseInt(notificationsColorLed);
            switch (selection){
                case 0:
                    ledText1.setText("80%");
                    ledText2.setText("50%");
                    ledText3.setText("20%");
                    break;
                case 1:
                    ledText1.setText("66%");
                    ledText2.setText("33%");
                    ledText3.setText("15%");
                    break;
                case 2:
                    ledText1.setText("50%");
                    ledText2.setText("25%");
                    ledText3.setText("10%");
                    break;
                case 3:
                    ledText1.setText("40%");
                    ledText2.setText("20%");
                    ledText3.setText("5%");
                    break;
                default:
                    break;
            }
        }else{ //...If LED notification is OFF
            ledIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
            ledIsSet.setImageDrawable(ledIcon);
            ledText1.setVisibility(View.GONE);
            ledText2.setVisibility(View.GONE);
            ledText3.setVisibility(View.GONE);
        }

        if(holdOnScreenFlag){ //If screen doesn't never turn off ...
            screenHoldOnIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFF1b5e20, PorterDuff.Mode.SRC_ATOP));
            screenHoldOnIsSet.setImageDrawable(screenHoldOnIcon);

        }else{ //...If it turns off according android system settings
            screenHoldOnIcon.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
            screenHoldOnIsSet.setImageDrawable(screenHoldOnIcon);
        }
    }

    /**
     * Set running countdown view
     */
    private void setRunningCountdownView(){
        //If the countdown has started set text button to stop instead reset
        vButtonStop.setText(getString(R.string.stop_button_text));
        vButtonStart.setVisibility(View.GONE);
        vButtonPause.setVisibility(View.VISIBLE);
        vButtonStop.setVisibility(View.VISIBLE);
        vButtonAdd.setVisibility(View.GONE);
        //Disable drawerLayout and toggle
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        //Enable flip clock countdown icon
        vFullScreenItem.setVisible(true);
        //During countdown we can not delete any time of recycler view
        itemTouchHelper.attachToRecyclerView(null);
    }

    /**
     * Set paused countdown view
     */
    private void setPauseCountdown(){
        vButtonStart.setVisibility(View.VISIBLE);
        vButtonStart.setText(getString(R.string.resume_button_text));
        vButtonPause.setVisibility(View.GONE);
    }

    public void setStopView(){
        vPeriodText.setVisibility(View.VISIBLE);
        vButtonStop.setText(getString(R.string.reset_button_text));
        vButtonPause.setVisibility(View.GONE);
        vButtonStart.setVisibility(View.VISIBLE);
        vButtonStart.setText(getString(R.string.start_button_text));
        vButtonAdd.setVisibility(View.VISIBLE);
        vPeriodPicker.setVisibility(View.VISIBLE);
        //Enable drawerLayout and show save button in action bar
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        //Disable countdown icon
        vFullScreenItem.setVisible(false);
        /* Show select time pickers loading animation if stop is pressed when countdown is running*/
        if(flagCountdownView){
            animateCountDown();
        }
        //Enable swipe delete on recyclerView when we come back from countdown
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    /**
     * Helps to choose if we need to animate so we can start countdown or viceversa
     */
    private void animateCountDown(){
        if (timeSetPicker.getX() <= 0) {
            animateStartCountdown();
            flagCountdownView = true;
        } else {
            animateFinishCountdown();
            flagCountdownView = false;
        }
    }
    /**
     * Text object animator method using Property Animation Google Framework to start countdown
     */
    private void animateStartCountdown(){
        //Close swipeLayout which is outside the screen
        swipeLayout.close(true);
        swipeLayout.setRightSwipeEnabled(false);
        //Set picker to translate going out through right screen during 600 ms
        ObjectAnimator pickerTranslationRight = ObjectAnimator.ofFloat(timeSetPicker, "X", timeSetPicker.getWidth());
        pickerTranslationRight.setDuration(600);
        /* Yet show time layout is inside a FrameLayout and we control its visibility according our needs,
           before we can show going in showTime view from left side of screen we need to translate out of the screen as fast as possible */
        ObjectAnimator timerTranslationRight = ObjectAnimator.ofFloat(showTime, "X", showTime.getWidth());
        timerTranslationRight.setDuration(0);
        //Then we can translate showtime view to translate going in from right side to left side of the screen during 600 ms
        ObjectAnimator timerTranslationLeft = ObjectAnimator.ofFloat(showTime, "X", 0f);
        timerTranslationLeft.setDuration(600);
        //Finally we start animations sequentially
        AnimatorSet translation = new AnimatorSet();
        translation.playSequentially(pickerTranslationRight, timerTranslationRight, timerTranslationLeft);
        translation.start();
        /* Set listener in order to set showTime visible when we have just translate out of the screen*/
        timerTranslationRight.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}
            @Override
            public void onAnimationEnd(Animator animation) {
                showTime.setVisibility(View.VISIBLE);
                timeSetPicker.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onAnimationCancel(Animator animation) {}
            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
         /* Prepare Countdown in the service. Placed in onAnimationStart so previous countdown
          * have time to reset and it can avoid to show previous countdown just before starts*/
        timerTranslationLeft.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                prepareStartCounting();
            }
            @Override
            public void onAnimationEnd(Animator animation) { }
            @Override
            public void onAnimationCancel(Animator animation) { }
            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
    }

    /**
     * Text object animator method using Property Animation Google Framework to finish countdown
     */
    private void animateFinishCountdown(){
        //Set showTimeView to translate going out through right screen during 600 ms
        ObjectAnimator timerTranslationRight = ObjectAnimator.ofFloat(showTime, "X", showTime.getWidth());
        timerTranslationRight.setDuration(600);
        //Set timeSetPicker view to show it translating it going in from right to left side of the screen during 600 ms
        ObjectAnimator pickerTranslationLeft = ObjectAnimator.ofFloat(timeSetPicker, "X", 0f);
        pickerTranslationLeft.setDuration(600);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(timerTranslationRight, pickerTranslationLeft);
        animatorSet.start();
        /* When The animation finish we need to hide showtime so user only can see time pickers */
        timerTranslationRight.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}
            @Override
            public void onAnimationEnd(Animator animation) {
                showTime.setVisibility(View.INVISIBLE);
                timeSetPicker.setVisibility(View.VISIBLE);
                swipeLayout.setRightSwipeEnabled(true);
            }
            @Override
            public void onAnimationCancel(Animator animation) {}
            @Override
            public void onAnimationRepeat(Animator animation) { }
        });
    }

    /**
     * Configure and Show tooltip to help the user
     * Tips are only shown if the user has not disable in preferences
     */
    private void showToolTips(View viewToTip) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        //Set if sound and vibration notifications are enabled
        final boolean tipsFlag = preferences.getBoolean("tips_notification", true);

        if (tipsFlag) {
            // Get ToolTipRelativeLayout to implement help tips with supertooltips library by nhaarman
            final ToolTipRelativeLayout toolTipRelativeLayout = (ToolTipRelativeLayout) view.findViewById(R.id.tooltip_frame_layout);
            // Inflate custom swipe to set interval tooltip layout
            View customTip = LayoutInflater.from(getActivity()).inflate(R.layout.custom_tooltip, null);
            // Variable to configure tooltips
            ToolTip toolTip = new ToolTip();
            //Variables that help to add listeners in irder to hide tips
            final ToolTipView swipeTip, addButtonTip;
            //Helps to chang text of the tooltip
            TextView text = (TextView) customTip.findViewById(R.id.text_tip);


            int id = viewToTip.getId();
            if (id == R.id.minutes_text_picker) {
                text.setText(getResources().getString(R.string.swipe_text_tip));
                //Configure tooltip
                toolTip.withContentView(customTip)
                        .withColor(Color.RED)
                        .withShadow()
                        .withAnimationType(ToolTip.AnimationType.FROM_TOP);
            /* Show tooltip and animate swipe icon*/
                swipeTip = toolTipRelativeLayout.showToolTipForView(toolTip, view.findViewById(R.id.minutes_text_picker));
                YoYo.with(Techniques.Shake).delay(0).duration(3000).playOn(customTip.findViewById(R.id.image_tip));

             /* Next lines configure tooltips in order to hide when it is necessary*/
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeTip.remove();
                    }
                }, 3000);

                swipeLayout.addSwipeListener(new SimpleSwipeListener() {
                    @Override
                    public void onStartOpen(SwipeLayout layout) {
                        super.onStartOpen(layout);
                        swipeTip.remove();
                    }
                });
            } else if (id == R.id.button_add) {
                text.setText(getResources().getString(R.string.add_button_text_tip));
                ImageView imageView = (ImageView) customTip.findViewById(R.id.image_tip);
                imageView.setVisibility(View.GONE);

                toolTip.withContentView(customTip)       // Avoid previous custom tooltip layout to set default
                        .withColor(Color.RED)
                        .withShadow()
                        .withTextColor(Color.WHITE)
                        .withAnimationType(ToolTip.AnimationType.FROM_TOP);
                addButtonTip = toolTipRelativeLayout.showToolTipForView(toolTip, vButtonAdd);

             /* Next lines configure tooltips in order to hide when it is necessary*/
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        addButtonTip.remove();
                    }
                }, 3000);
            }
        }
    }

    /**
     * Callback connected to ManageNotificationService class in order to manage countdown
     * from the notification action buttons
     * @param action
     */
    @Override
    public void updateState(String action) {
        switch (action){
            case Constants.ACTION.PAUSE_ACTION:
                MyLogger.d("TimeService", "Pause Button clicked and getting into fragment");
                onClick(vButtonPause);
                break;
            case Constants.ACTION.PLAY_ACTION:
                MyLogger.d("TimeService", "Play Button clicked and getting into fragment");
                onClick(vButtonStart);
                break;
            case Constants.ACTION.STOP_ACTION:
                MyLogger.d("TimeService", "STOP Button clicked and getting into fragment");
                onClick(vButtonStop);
                break;
            default:
                MyLogger.d("TimeService", "NO Button clicked and getting into fragment");
                break;
        }
    }

    public boolean isCountDownPaused(){
        return flagCountdownPause;
    }

    /**
     * Implement Swipe delete on RecyclerView List.
     */
    private ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            deleteOrAddTimes(0, viewHolder.getAdapterPosition(), true);
        }
    };




}//End of class
