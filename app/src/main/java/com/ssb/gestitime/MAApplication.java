/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ssb.gestitime;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/** gestitime
 * Created by sergio on 22/05/16.
 * set font selected as default in all the application
 */
public class MAApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Set The default font for the entirely application
        CalligraphyConfig.initDefault(new CalligraphyConfig
                .Builder()
                .setDefaultFontPath("fonts/DroidSansMono.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}
