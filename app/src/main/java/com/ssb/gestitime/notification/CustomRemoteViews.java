/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.ssb.gestitime.notification;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.RemoteViews;

import com.ssb.gestitime.ManageActionsNotificationService;
import com.ssb.gestitime.R;
import com.ssb.gestitime.TimerService;
import com.ssb.gestitime.utils.Constants;

/**
 * CustomRemoteView
 * GestiTime Created by Sergio Sánchez Barahona on 1/08/16.
 * Simplifies the creation of a RemoteView avoiding the need to be setting
 * onClickListening of the Notification actions button every time the class is instantiated
 */
@SuppressLint("ParcelCreator")
public class CustomRemoteViews extends RemoteViews {

    private final Context mContext;

    public CustomRemoteViews(Context context, String packageName, int layoutId) {
        super(packageName, layoutId);
        this.mContext = context;

        /* Configure custom layout and their buttons. Notification's Buttons are manage from an auxiliary service */
        Intent pauseIntent = new Intent(mContext, ManageActionsNotificationService.class);
        //Set PAUSE Button
        pauseIntent.setAction(Constants.ACTION.PAUSE_ACTION);
        PendingIntent pausePendingIntent = PendingIntent.getService(mContext.getApplicationContext(), TimerService.NOTIFICATION_ID, pauseIntent, 0);
        setOnClickPendingIntent(R.id.notification_pause_button, pausePendingIntent);

        //Set PLAY Button
        Intent playIntent = new Intent(mContext, ManageActionsNotificationService.class);
        playIntent.setAction(Constants.ACTION.PLAY_ACTION);
        PendingIntent playPendingIntent = PendingIntent.getService(mContext.getApplicationContext(), TimerService.NOTIFICATION_ID, playIntent, 0);
        setOnClickPendingIntent(R.id.notification_play_button, playPendingIntent);

        //Set STOP Button
        Intent stopIntent = new Intent(mContext, ManageActionsNotificationService.class);
        stopIntent.setAction(Constants.ACTION.STOP_ACTION);
        PendingIntent stopPendingIntent = PendingIntent.getService(mContext.getApplicationContext(), TimerService.NOTIFICATION_ID, stopIntent, 0);
        setOnClickPendingIntent(R.id.notification_stop_button, stopPendingIntent);

        if (Build.VERSION.SDK_INT >= 16) {
            setOnClickPendingIntent(R.id.notification_pause_button_text, pausePendingIntent);
            setOnClickPendingIntent(R.id.notification_play_button_text, playPendingIntent);
        }
    }
}
