/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ssb.gestitime;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.ssb.gestitime.fragment.MainFragment;
import com.ssb.gestitime.databases.Ctime;
import com.ssb.gestitime.notification.CustomRemoteViews;
import com.ssb.gestitime.utils.Constants;
import com.ssb.gestitime.utils.MyLogger;
import com.ssb.gestitime.utils.TimeUtility;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * gestitime
 * 18/05/2016. Sergio Sánchez Barahona. sesaba23@gmail.com
 *
 * This class service implements a countdown timer.
 * It implements a binder, so classes can bind it so other classes could access public methods of the class.
 * It implements a callback in order to update the visual countdown in the UI
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 *
 */
public class TimerService extends IntentService {

    /**
     * Binder implementation overriding its onBind method
     */
    public class TimerBinder extends Binder {

        public TimerService getTimerService() {
            MyLogger.d("HowServicesWork", "MainFragment getting timer service");
            return TimerService.this;
        }
    }
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    /**
     * Interface to communicate with fragment/activity's service
     */
    public interface CallBacks{
        void updateClient(long seconds, int i);
    }

    /**
     * Global variables
     */
    /* Static final variables to share information throw intents*/
    public static final String NUM_NOTIFICATIONS = "numNotifications";
    public static final String SECONDS = "seconds";
    public static final String ARRAY = "array";
    private int MUCHTIME = 66;
    private int MIDDLETIME = 33;
    private int FINISHINGTIME = 10;

    private final IBinder binder;
    private Intent intent;

    //Variables to implement the counter
    private long seconds = 0;
    private ArrayList<Ctime> CtimesArray; //Stores times which are going to be shown in the listview
    private int numNotifications = 0;

    private ArrayList<CallBacks> callback;

    //Helps to identify notification
    public static final int NOTIFICATION_ID = 5453;
    private NotificationCompat.Builder builder;
    private NotificationManager notificationManager;
    private Notification notification;

    private boolean serviceStarted;
    private boolean counterStarted;

    private CountDownTimer countDownTimer;

    private SharedPreferences preferences;
    private int timeNotificationVibration;

    private boolean flagConfigure;

    private String actualTime;

    /**
     * Constructor
     */
    public TimerService() {
        super("Timerservice");
        //Create a binder to join service with fragment
        binder = new TimerBinder();
        callback = new ArrayList<>();
        serviceStarted = false;
        counterStarted = false;
        flagConfigure = false;
        MyLogger.d("HowServicesWork", "Constructing timeService");
    }

    /**
     * Runs when Intents service is started on the main Thread
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        this.intent = intent;
        MyLogger.d("HowServicesWork", "Starting timeService");
        //Reference to control notification preferences
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        //Get the information typed in the UI
        seconds = intent.getLongExtra(SECONDS, 0);
        numNotifications = intent.getIntExtra(NUM_NOTIFICATIONS, 0);
        CtimesArray = intent.getParcelableArrayListExtra(ARRAY);
            if(!flagConfigure) {
                //Notifications are configure only when the service is created
                configureCustomNotification();
                flagConfigure = true;
            }else{ //When we start countdown from PAUSE we only need to put service in foreground again
                startForeground(NOTIFICATION_ID, notification);
            }
        //Run the timer countdown
        counterStarted = true;
        newRunTimer();

        //Every time the service is started (from stop or pause) show only pause button
        CustomRemoteViews contentView = new CustomRemoteViews(this, getPackageName(), R.layout.notification);
        contentView.setViewVisibility(R.id.notification_pause_button, View.VISIBLE);
        contentView.setViewVisibility(R.id.notification_play_button, View.GONE);

        if (Build.VERSION.SDK_INT >= 16) {
            CustomRemoteViews expandedView =
                    new CustomRemoteViews(this, getPackageName(), R.layout.notification_expanded);
            expandedView.setViewVisibility(R.id.notification_pause_button, View.VISIBLE);
            expandedView.setViewVisibility(R.id.notification_pause_button_text, View.VISIBLE);
            expandedView.setViewVisibility(R.id.notification_play_button, View.GONE);
            expandedView.setViewVisibility(R.id.notification_play_button_text, View.GONE);
            expandedView.setTextViewText(R.id.notification_play_button_text, getString(R.string.resume_button_text));
            builder.setCustomBigContentView(expandedView);
        }
        builder.setContent(contentView);

        //Get the start time of the countdown
        actualTime = intent.getStringExtra(Constants.ACTION.ACTUAL_TIME);

        return START_STICKY;
    }

    /**
     * Runs after onStart Command but in a background thread (notification in the UIThread (main))
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        //Help to execute threads like a FIFO stack
        synchronized (this){
        }
    }

    /**
     * Implement the cont down and update the view
     */
    public void newRunTimer(){

        MyLogger.d("TimeService", "Creating newRunTimer");
        /* Wakelock in order to run the service even in the sleep mode. The CPU doesn't stop during counting */
        PowerManager pm = (PowerManager)getSystemService(getApplicationContext().POWER_SERVICE);
        final PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        /* Add 100 msg in order to avoid last onTick as a result of delay method in CountDownTimer class*/
        countDownTimer = new CountDownTimer((seconds) * 1000 + 100, 1000){

            final Vibrator v = (Vibrator) getApplicationContext().getSystemService(getApplicationContext().VIBRATOR_SERVICE);

            final long totalMilliseconds = seconds * 1000;

            @Override
            public void onTick(long millisUntilFinished) {
                //Get Notification Preferences
                final boolean notificationsFlag = preferences.getBoolean("notifications_new_message", true);
                final boolean VibrationFlag = preferences.getBoolean("notification_vibration", true);

                timeNotificationVibration = Integer.parseInt(preferences.getString("time_notification_vibration", "300"));

                //Set notifications sound. If user has choose "silent" in preferences, don't play any sound
                String soundChoose = preferences.getString("notifications_ringtone", "");
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), Uri.parse(soundChoose));

                long seconds = millisUntilFinished / 1000;

                long timeForNextNotification = CtimesArray.get(CtimesArray.size()-1).getTimeSeconds() - CtimesArray.get(numNotifications).getTimeSeconds();

                double progress = (double) millisUntilFinished / (double) totalMilliseconds * 100.00;
                ledNotification(progress);

                MyLogger.d("watchSecondsHandler", "OnTick is: " + millisUntilFinished + " ms and " + seconds + " s");
                for(int i = 0; i < callback.size(); i++) {
                    MyLogger.d("ChangeConfig", "Time Sevice CTimes Array size: " + CtimesArray.size());
                    callback.get(i).updateClient(seconds, numNotifications);
                }

                //If time equals some of the times in the listView warn the user
                if(seconds == timeForNextNotification){
                    numNotifications++;
                    Toast.makeText(getApplicationContext(), getString(R.string.time_left) + " " + TimeUtility.convertSecondsInTime(seconds), Toast.LENGTH_SHORT).show();
                    if(VibrationFlag & notificationsFlag) { //Only vibrate if the user has selected in preferences
                        v.vibrate(new long[]{0, timeNotificationVibration}, -1); // Vibrate for a number of milliseconds
                    }
                    //Play sound if the user doesn't choose silent in preferences
                    if(!soundChoose.equals("") & notificationsFlag){
                        r.play(); //Play sound choose in preference
                    }
                }

                //Update the notification in the actionbar and lock screen
                showCustomNotification(seconds, TimeUtility.convertSecondsInTime(seconds - timeForNextNotification), numNotifications);
            }
            @Override
            public void onFinish() {
                counterStarted = false;
                MyLogger.d("TimeService", "Finish countDownTimer");
                Toast.makeText(getApplicationContext(), getString(R.string.time_end), Toast.LENGTH_SHORT).show();
                numNotifications++;
                flagConfigure = false; //Next time we start countdown reset end notification and configure countdown notification
                for(int i = 0; i < callback.size(); i++) {
                    callback.get(i).updateClient(0, 0);
                }
                showEndNotification(getString(R.string.time_end));
                wl.release(); //Release the wakelock
            }
        }.start();
    }

    private void configureCustomNotification(){
        MyLogger.d("TimeService", "Configuring Notifications");

        // Set text on a TextView in the RemoteViews programmatically.
        final String time = DateFormat.getTimeInstance().format(new Date());

        // 1.- Create a explicit intent directed to the activity you want to start when the notification is clicked
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        // 2.- Create a stackBuilder to make sure that the back button will play nicely when the activity gets started
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // 3.- Set the back stack related to the activity
        stackBuilder.addParentStack(MainActivity.class);
        // 4.- Add the intent to the back stack
        stackBuilder.addNextIntent(intent);
        // 5.- Get the pending intent from the TaskStackBuilder
/*        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, //Request code that can be used to identify the intent
                PendingIntent.FLAG_UPDATE_CURRENT); //Flag that specifies the pending intent's behavior.*/
        //If a matching pending intent exists, keep it and replace its extra data with the contents in the intent

        //Get current running activity and set to the pending intent to go back to main UI when notification is clicked
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), NOTIFICATION_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // 6.- Creates a notification manager object to access Android's notification service
        notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        // 7.- Create a notification object content details of how the notification should be configured
        builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_notification)
                .setPriority(Notification.PRIORITY_MAX)
                .setAutoCancel(false)
                .setOngoing(true)
                .setContentIntent(pendingIntent); //Pass the pending intent
        if(Build.VERSION.SDK_INT >= 21){
            builder.setCategory(Notification.CATEGORY_ALARM);
        }

        notification = builder.build();
        startForeground(NOTIFICATION_ID, notification);
    }

    /**
     * Implement notification service in the action bar
     */
    private void showCustomNotification(final long seconds, final String timeNextNotification, Integer count){
        MyLogger.d("TimeService", "Show Notification");
        String timeLeft = TimeUtility.convertSecondsInTime(seconds);
        //Set every textView of the custom notification layout
        CustomRemoteViews contentView = new CustomRemoteViews(this, getPackageName(), R.layout.notification);
        contentView.setTextViewText(R.id.notification_left_time, timeLeft);
        contentView.setTextViewText(R.id.next_notification_left_time, timeNextNotification);
        //Set content in the notification builder
        builder.setContent(contentView);

        if (Build.VERSION.SDK_INT >= 16) {
            CustomRemoteViews expandedView =
                    new CustomRemoteViews(this, getPackageName(), R.layout.notification_expanded);
            expandedView.setTextViewText(R.id.notification_left_time, timeLeft);
            expandedView.setTextViewText(R.id.next_notification_left_time, timeNextNotification);
            expandedView.setTextViewText(R.id.current_number_of_notification, count.toString());
            expandedView.setTextViewText(R.id.total_number_of_notifications, ((Integer)CtimesArray.size()).toString());
            expandedView.setTextViewText(R.id.elapse_time_notification,
                    TimeUtility.convertSecondsInTime(CtimesArray.get(CtimesArray.size()-1).getTimeSeconds() - seconds));
            //Show the started time of the countdown
            expandedView.setTextViewText(R.id.started_time_notification, actualTime);
            builder.setCustomBigContentView(expandedView);
        }

        notification = builder.build();
        //Show Notification
        notificationManager.notify(NOTIFICATION_ID, notification);
    }

    /**
     * Implement notification service in the action bar
     */
    private void ledNotification(double count){
        final boolean ledFlag = preferences.getBoolean("led_notification", true);

        setSelectColorLedTimes();
        if(ledFlag) {
            if (count > MUCHTIME)
                builder.setLights(Color.GREEN, 1000, 6000);
            else if (count > MIDDLETIME)
                builder.setLights(Color.YELLOW, 1000, 3000);
            else if (count > FINISHINGTIME)
                builder.setLights(Color.parseColor("#FF6D00"), 300, 300); //Orange
            else
                builder.setLights(Color.RED, 100, 100);
        }
        else{
            builder.setLights(Color.GREEN, 0, 0);
        }
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    private void showEndNotification(final String information){
        MyLogger.d("TimeService", "Show END Notification");
        //Get Notification Preferences
        final boolean vibrationFlag = preferences.getBoolean("finished_vibration", true);
        final boolean notificationsFlag = preferences.getBoolean("notifications_new_message", true);
        final boolean ledFlag = preferences.getBoolean("led_notification", true);
        final String sound =preferences.getString("end_notifications_ringtone", "");

            builder.setOngoing(false)
                    .setAutoCancel(true);
                if(vibrationFlag & notificationsFlag) {
                    builder.setVibrate(new long[]{0, 100, 100, 100, 200, 600, 100, 600, 0, 100, 100, 100, 200, 600, 100, 600});
                }
                if(!sound.equals("") & notificationsFlag) {
                    builder.setSound(Uri.parse(sound));
                }
                if(ledFlag) {
                    builder.setLights(Color.RED, 100, 0);
                }

        CustomRemoteViews contentView = new CustomRemoteViews(this, getPackageName(), R.layout.notification);
        contentView.setTextViewText(R.id.notification_left_time_text, information);
        contentView.setViewVisibility(R.id.notification_left_time, View.GONE);
        contentView.setViewVisibility(R.id.next_notification_left_time_text, View.VISIBLE);
        contentView.setTextViewText(R.id.next_notification_left_time_text, getString(R.string.notification_time_finished_text));
        contentView.setViewVisibility(R.id.next_notification_left_time, View.VISIBLE);
        contentView.setTextViewText(R.id.next_notification_left_time, TimeUtility.getCurrentTime());
        contentView.setViewVisibility(R.id.notification_pause_button, View.GONE);
        contentView.setViewVisibility(R.id.notification_stop_button, View.GONE);
        contentView.setViewVisibility(R.id.notification_play_button, View.GONE);

        if (Build.VERSION.SDK_INT >= 16) {
            CustomRemoteViews expandedView =
                    new CustomRemoteViews(this, getPackageName(), R.layout.notification_expanded);
            expandedView.setTextViewText(R.id.notification_left_time_text, information);
            expandedView.setViewVisibility(R.id.notification_left_time, View.GONE);

            expandedView.setTextViewText(R.id.current_number_of_notification, ((Integer)numNotifications).toString());
            expandedView.setTextViewText(R.id.total_number_of_notifications, ((Integer)CtimesArray.size()).toString());

            expandedView.setTextViewText(R.id.next_notification_left_time_text, getString(R.string.notification_time_finished_text));
            expandedView.setTextViewText(R.id.next_notification_left_time, TimeUtility.getCurrentTime());
            expandedView.setTextViewText(R.id.elapse_time_notification, CtimesArray.get(CtimesArray.size()-1).getTimeString());
            expandedView.setTextViewText(R.id.notification_play_button_text, getString(R.string.notification_restart_button_text));
            expandedView.setTextViewText(R.id.started_time_notification, actualTime);

            expandedView.setViewVisibility(R.id.notification_pause_button, View.GONE);
            expandedView.setViewVisibility(R.id.notification_pause_button_text, View.GONE);
            expandedView.setViewVisibility(R.id.notification_play_button, View.VISIBLE);
            expandedView.setViewVisibility(R.id.notification_play_button_text, View.VISIBLE);
            builder.setCustomBigContentView(expandedView);
        }

        builder.setContent(contentView);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
        stopForeground(false);
    }

    /**
     * Stops the countdown and the service
     */
    public void stopTimer(String action){
        MyLogger.d("TimeService", "stopTimer timeService");
        numNotifications = 0;
        serviceStarted = false;
        counterStarted = false;
        countDownTimer.cancel();
//        handler.removeCallbacks(serviceRunnable);

        CustomRemoteViews contentView = new CustomRemoteViews(this, getPackageName(), R.layout.notification);
        contentView.setViewVisibility(R.id.notification_pause_button, View.GONE);
        contentView.setViewVisibility(R.id.notification_play_button, View.VISIBLE);
        contentView.setViewVisibility(R.id.notification_stop_button, View.VISIBLE);
        if (Build.VERSION.SDK_INT >= 16) {
            CustomRemoteViews expandedView =
                    new CustomRemoteViews(this, getPackageName(), R.layout.notification_expanded);
            expandedView.setViewVisibility(R.id.notification_pause_button, View.GONE);
            expandedView.setViewVisibility(R.id.notification_pause_button_text, View.GONE);
            expandedView.setViewVisibility(R.id.notification_play_button, View.VISIBLE);
            expandedView.setViewVisibility(R.id.notification_play_button_text, View.VISIBLE);
            builder.setCustomBigContentView(expandedView);
        }
        builder.setContent(contentView);

        notification = builder.build();
        //If we press stop cancel notification and make service stop in foreground
        if(action.equalsIgnoreCase(Constants.ACTION.STOP_ACTION)) {
            notificationManager.cancel(NOTIFICATION_ID);
            MyLogger.d("TimeService", "STOPTimer intent received: " + action);
            stopForeground(true);
        }// If we press pause notification is still visible and ongoing
        else {
            notificationManager.notify(NOTIFICATION_ID, notification);
            MyLogger.d("TimeService", "stopTimer intent received: " + action);
        }
    }

    /**
     *  Join callback with fragment in order to share information when we want to update interface
     **/
    public void registerClient(MainFragment fragment){
        /* Watch out! Stop service is necessary in order to unregisterClient making callback ArrayList clear.
         * If not, we need to clear ArrayList manually in this method or we add register clients getting an error */
        //        this.callback.clear();
        this.callback.add(fragment);
        MyLogger.d("HowServicesWork", "Register MainFragment as an Observer. CallBack size: " + callback.size());
    }

    /**
     *  Join callback with fragment in order to share information when we want to update interface
     **/
    public void registerClient(CountDownFullscreenActivity activity){
        if(this.callback.size() == 1) {
            this.callback.add(activity);
        }else{
            this.callback.set(1, activity);
        }
        MyLogger.d("HowServicesWork", "Register FullScreen as an Observer. CallBack size: " + callback.size());
    }

    @Override
    public boolean stopService(Intent name) {
        MyLogger.d("HowServicesWork", "Service Stopped");
        return super.stopService(name);
    }

    public boolean isServiceStarted(){
        return serviceStarted;
    }

    public boolean isCountDownStarted(){
        return counterStarted;
    }

    /**
     * Set the color led according % times
     */
    private void setSelectColorLedTimes(){
        String notificationsColorLed = preferences.getString("led_time_notification", "0");
        int selection = Integer.parseInt(notificationsColorLed);
        switch (selection){
            case 0:
                MUCHTIME = 80;
                MIDDLETIME = 50;
                FINISHINGTIME = 20;
                break;
            case 1:
                MUCHTIME = 66;
                MIDDLETIME = 33;
                FINISHINGTIME = 15;
                break;
            case 2:
                MUCHTIME = 50;
                MIDDLETIME = 25;
                FINISHINGTIME = 10;
                break;
            case 3:
                MUCHTIME = 40;
                MIDDLETIME = 20;
                FINISHINGTIME = 5;
                break;
            default:
                break;
        }
    }


}//End of class

