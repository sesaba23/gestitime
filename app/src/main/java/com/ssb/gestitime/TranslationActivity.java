/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ssb.gestitime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 20/07/2016. gestitime. Sergio Sánchez barahona
 */
public class TranslationActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView sendEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translation);

         /* Set and configure send email to developers to get underline and execute email app*/
        sendEmail = (TextView)findViewById(R.id.send_mail_text);
        SpannableString content = new SpannableString(getString(R.string.developer_email));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        sendEmail.setText(content);
        sendEmail.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view == sendEmail)
        {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("plain/text");
            i.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.developer_email)});
            try {
                startActivity(i);
                overridePendingTransition(R.anim.open_next, R.anim.close_main);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(), "There are no email applications installed.", Toast.LENGTH_LONG).show();
            }
        }else {
        /* Open legal and third party Activity*/
            Intent intent = new Intent(this, LegalInformationActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.open_next, R.anim.close_main);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition (R.anim.open_main, R.anim.close_next);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition (R.anim.open_main, R.anim.close_next);
            return true;
        }
        return false;
    }
}
