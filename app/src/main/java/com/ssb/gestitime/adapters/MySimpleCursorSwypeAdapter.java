/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ssb.gestitime.adapters;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.SimpleCursorSwipeAdapter;
import com.ssb.gestitime.MainActivity;
import com.ssb.gestitime.R;
import com.ssb.gestitime.databases.CgestitimeDataBaseHelper;
import com.ssb.gestitime.databases.DatabaseContract;

/**
 * gestitime
 * 15/06/2016. Sergio Sánchez Barahona. sesaba23@gmail.com
 * A custom SimpleCursorAdapter to show records and manage events like delete record and
 * close swipelayouts
 */
public class MySimpleCursorSwypeAdapter extends SimpleCursorSwipeAdapter{

    private Context mContext;
    private int position;
    private Cursor cursor;
    private ImageButton swipeDelete;
    private ImageView crossDelete;

    public MySimpleCursorSwypeAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        this.mContext = context;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_layout;
    }

    @Override
    public void closeAllItems() {
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {
        super.bindView(view, context, cursor);
        final int position = cursor.getPosition();
        final String itemId = cursor.getString(cursor.getColumnIndex(DatabaseContract.RecordTimers._ID));
        final String itemName = cursor.getString(cursor.getColumnIndex(DatabaseContract.RecordTimers.NAME_RECORD));

        //Set animation icons when swipe on a record
        SwipeLayout swipeLayout = (SwipeLayout) view.findViewById(R.id.swipe_layout);
        swipeLayout.addSwipeListener(new SimpleSwipeListener(){
            @Override
            public void onOpen(SwipeLayout layout) {
                super.onOpen(layout);
                YoYo.with(Techniques.Tada).duration(1000).delay(100).playOn(layout.findViewById(R.id.delete_record));
                YoYo.with(Techniques.Shake).duration(1000).delay(1000).playOn(layout.findViewById(R.id.close_swipe));
            }
        });


        /* When pressed the trash icon delete saved record in the dataBase*/
        swipeDelete = (ImageButton) view.findViewById(R.id.delete_record);
        swipeDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, itemName + mContext.getString(R.string.record_deleted), Toast.LENGTH_LONG).show();
                deleteRecordWithId(itemId);
                cursor.requery();
                notifyDataSetChanged();
                closeItem(position);

            }
        });

        crossDelete = (ImageView) view.findViewById(R.id.cross_delete_record);
        crossDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, itemName + mContext.getString(R.string.record_deleted), Toast.LENGTH_LONG).show();
                deleteRecordWithId(itemId);
                cursor.requery();
                notifyDataSetChanged();
            }
        });

        /* When User has just done a left swype in order to delete some record
        * but finally he regrets, he can click on the close_swipe ImageButton again to close delete icon*/
        final ImageButton l = (ImageButton) view.findViewById(R.id.close_swipe);
        final SwipeLayout s = (SwipeLayout) view.findViewById(R.id.swipe_layout);
        l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(s.getOpenStatus() == SwipeLayout.Status.Open) {
                    closeItem(position);
                }
            }
        });

        /* Set drawer listener in order to close the swipelayout selected */
        MainActivity mainActivity = (MainActivity) mContext;
        final DrawerLayout drawerLayout = (DrawerLayout) mainActivity.findViewById(R.id.drawer_layout);
        if(drawerLayout != null) {
            drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) { }
                @Override
                public void onDrawerOpened(View drawerView) { }
                @Override
                public void onDrawerClosed(View drawerView) {
                    closeItem(position);
                }
                @Override
                public void onDrawerStateChanged(int newState) { }
            });
        }

    }


    private void deleteRecordWithId(String itemId){
        CgestitimeDataBaseHelper gt = new CgestitimeDataBaseHelper(mContext);
        SQLiteDatabase db = gt.getWritableDatabase();

        db.delete(DatabaseContract.RecordTimers.TABLE_NAME, "_id = ?", new String[]{itemId});
        db.close();
    }
}
