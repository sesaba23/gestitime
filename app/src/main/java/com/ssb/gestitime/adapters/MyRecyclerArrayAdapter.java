/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.ssb.gestitime.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ssb.gestitime.R;
import com.ssb.gestitime.databases.Ctime;
import com.ssb.gestitime.utils.TimeUtility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * gestitime
 * Created by Sergio Sánchez Barahona on 18/05/16.
 */
public class MyRecyclerArrayAdapter extends RecyclerView.Adapter<MyRecyclerArrayAdapter.ViewHolder> {

    private Context mContext;
    private int row;
    private ArrayList<Ctime> times;
    private int actualPosition = 0;
    private boolean flagAdded = false;


    public MyRecyclerArrayAdapter(Context context, int resource, List<Ctime> objects) {
        this.mContext = context;
        this.row = resource;
        this.times = (ArrayList<Ctime>) objects;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(row, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Ctime ctime = times.get(position);

        holder.notificationTimeView.setText(ctime.getTimeString());
        holder.timeLeftItemView.setText(TimeUtility.convertSecondsInTime(ctime.getTimeLeft()));
        holder.progressBar.setMax((int)ctime.getTimeSeconds());
        holder.progressBar.setProgress((int)ctime.getTimeLeft());
        //Animate
        setAnimation(holder.container, position);

        if(!ctime.isCompleted()){
            holder.notificationTimeView.setTextColor(mContext.getResources().getColor(R.color.materialRed900));
            Drawable drawable = mContext.getResources().getDrawable(R.mipmap.ic_timer_black_18dp);
            drawable.mutate().setColorFilter(new PorterDuffColorFilter(0xFF303F9F, PorterDuff.Mode.SRC_ATOP));
            holder.imageViewStopwatch.setImageDrawable(drawable);

            drawable = mContext.getResources().getDrawable(R.mipmap.ic_add_alert_black_18dp);
            drawable.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
            holder.imageViewBell.setImageDrawable(drawable);
            holder.flagAnimation = false;

        }
        else{
            holder.notificationTimeView.setTextColor(mContext.getResources().getColor(R.color.materialGreen900));
            Drawable drawable = mContext.getResources().getDrawable(R.mipmap.ic_timer_black_18dp);
            drawable.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
            holder.imageViewStopwatch.setImageDrawable(drawable);

            drawable = mContext.getResources().getDrawable(R.mipmap.ic_add_alert_black_18dp);
            drawable.mutate().setColorFilter(new PorterDuffColorFilter(0xFF1b5e20, PorterDuff.Mode.SRC_ATOP));
            holder.imageViewBell.setImageDrawable(drawable);

//            //Only animate the row of list view that correspond
//            if(!holder.flagAnimation) {
//                YoYo.with(Techniques.Flash).duration(1000).playOn(holder.imageViewStopwatch);
//                YoYo.with(Techniques.Shake).duration(1000).playOn(holder.imageViewBell);
//                holder.flagAnimation = true;
//            }
        }
    }

    @Override
    public int getItemCount() {
        return times.size();
    }

    /**
     * To apply viewHolder pattern
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView notificationTimeView;
        public TextView timeLeftItemView;
        public ImageView imageViewStopwatch, imageViewBell;
        public ProgressBar progressBar;
        public boolean flagAnimation;
        public CardView container;

        public ViewHolder(View view) {
            super(view);

            container = (CardView)view.findViewById(R.id.item_layout_container);

            notificationTimeView = (TextView) view.findViewById(R.id.time_item_listview);
            timeLeftItemView = (TextView) view.findViewById(R.id.time_left_item_listview);
            imageViewStopwatch = (ImageView) view.findViewById(R.id.stopwatch_image_item_listview);
            imageViewBell = (ImageView) view.findViewById(R.id.bell_image_item_listview);
            progressBar = (ProgressBar) view.findViewById(R.id.progressbar_item_listview);
            flagAnimation = false;
        }
    }

    /**
     *  Use to update data avoiding having to restart adapter when data change.
     *  Also helps to maintain scroll position
     */
    public void updateData(List<Ctime> objects){
        this.times = (ArrayList<Ctime>) objects;
        flagAdded = false;
        this.notifyDataSetChanged();
    }


    public void deleteData(int position){
        times.remove(position);
            notifyItemRemoved(position);
    }

    public void addData(Ctime ctime){
        times.add(ctime);
//        Sort CtimesArray by ascending seconds
            Collections.sort(times, new Comparator<Ctime>() {
                @Override
                public int compare(Ctime lhs, Ctime rhs) {
                    return new Long(lhs.getTimeSeconds()).compareTo(new Long(rhs.getTimeSeconds()));
                }
            });
        // We store the new position of the time added to array in order to animate only inserted row when on bind view
        flagAdded = true;
        actualPosition = times.indexOf(ctime);
        notifyDataSetChanged();
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position == actualPosition & flagAdded) {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
        }
    }


    public void animateCompletedRow(int position){

    }

}
