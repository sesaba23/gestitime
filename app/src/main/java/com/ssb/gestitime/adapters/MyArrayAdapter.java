/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ssb.gestitime.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.ssb.gestitime.R;
import com.ssb.gestitime.databases.Ctime;
import com.ssb.gestitime.utils.TimeUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * gestitime
 * Created by Sergio Sánchez Barahona on 18/05/16.
 */
public class MyArrayAdapter extends ArrayAdapter<Ctime> {

    private Context mContext;
    private int row;
    private ArrayList<Ctime> times;

    /**
     * To apply viewHolder pattern
     */
    private class ViewHolder{
        public TextView itemView;
        public TextView timeLeftItemView;
        public ImageView imageViewStopwatch, imageViewBell;
        public ProgressBar progressBar;
        public boolean flagAnimation;

        public ViewHolder(View view) {
            this.itemView = (TextView)view.findViewById(R.id.time_item_listview);
            this.timeLeftItemView =(TextView)view.findViewById(R.id.time_left_item_listview);
            this.imageViewStopwatch = (ImageView)view.findViewById(R.id.stopwatch_image_item_listview);
            this.imageViewBell = (ImageView)view.findViewById(R.id.bell_image_item_listview);
            this.progressBar = (ProgressBar)view.findViewById(R.id.progressbar_item_listview);
            this.flagAnimation = false;
        }

        public void bindView(Ctime ctime){
            this.itemView.setText(ctime.getTimeString());
            this.timeLeftItemView.setText(TimeUtility.convertSecondsInTime(ctime.getTimeLeft()));
            this.progressBar.setMax((int)ctime.getTimeSeconds());
            this.progressBar.setProgress((int)ctime.getTimeLeft());

            if(!ctime.isCompleted()){
                itemView.setTextColor(mContext.getResources().getColor(R.color.materialRed900));
                Drawable drawable = mContext.getResources().getDrawable(R.mipmap.ic_timer_black_18dp);
                drawable.mutate().setColorFilter(new PorterDuffColorFilter(0xFF303F9F, PorterDuff.Mode.SRC_ATOP));
                imageViewStopwatch.setImageDrawable(drawable);

                drawable = mContext.getResources().getDrawable(R.mipmap.ic_add_alert_black_18dp);
                drawable.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
                imageViewBell.setImageDrawable(drawable);
                flagAnimation = false;

            }
            else{
                itemView.setTextColor(mContext.getResources().getColor(R.color.materialGreen900));
                Drawable drawable = mContext.getResources().getDrawable(R.mipmap.ic_timer_black_18dp);
                drawable.mutate().setColorFilter(new PorterDuffColorFilter(0xFFb71c1c, PorterDuff.Mode.SRC_ATOP));
                imageViewStopwatch.setImageDrawable(drawable);

                drawable = mContext.getResources().getDrawable(R.mipmap.ic_add_alert_black_18dp);
                drawable.mutate().setColorFilter(new PorterDuffColorFilter(0xFF1b5e20, PorterDuff.Mode.SRC_ATOP));
                imageViewBell.setImageDrawable(drawable);

                //Only animate the row of list view that correspond
                if(!flagAnimation) {
                    YoYo.with(Techniques.Flash).duration(1000).playOn(imageViewStopwatch);
                    YoYo.with(Techniques.Shake).duration(1000).playOn(imageViewBell);
                    flagAnimation = true;
                }
            }
        }
    }

    public MyArrayAdapter(Context context, int resource, List<Ctime> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.row = resource;
        this.times = (ArrayList<Ctime>) objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null){
            convertView = View.inflate(mContext, R.layout.recycler_row, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.bindView(times.get(position));

        return convertView;
    }

    /**
     *  Use to update data avoiding having to restart adapter when data change.
     *  Also helps to maintain scroll position
     */
    public void updateData(List<Ctime> objects){
        this.times = (ArrayList<Ctime>) objects;
        this.notifyDataSetChanged();
    }
}
