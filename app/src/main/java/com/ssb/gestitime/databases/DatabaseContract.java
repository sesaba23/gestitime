/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ssb.gestitime.databases;

/**
 * gestitime
 * 15/06/2016. Sergio Sánchez Barahona. sesaba23@gmail.com
 *
 * Defines the name of the database and the name for all the tables and columns as constants
 * It serves as an idea of the structure of the database and avoid mistakes allowing us to make
 * only one change in the value of the constant and propagate over our entire app
 */
public class DatabaseContract {

    public static final String DB_NAME = "gestitime_timers"; //The name of the data base"

    /**
     * Static Global Variables to share information between Intents
     **/
    public static final String SELECTEDRECORD = "selectedrecord"; //Store selected short of street in TopLevelActivity


    /**
     * Database record timers
     */
    public abstract class RecordTimers{
        public static final String TABLE_NAME = "RECORD_TIMERS";
        public static final String _ID = "_id";
        public static final String NAME_RECORD = "NAME_RECORD";
        public static final String DESCRIPTION_RECORD = "DESCRIPTION_RECORD";
        public static final String ID_USER = "ID_USER";
    }

    /**
     * Database LIST record timers
     */
    public abstract class ListRecordTimers {
        public static final String TABLE_NAME = "LIST_RECORD_TIMERS";
        public static final String _ID = "_id";
        public static final String ID_RECORD = "ID_RECORD";
        public static final String TIME_ITEM = "TIME_ITEM";
    }

}
