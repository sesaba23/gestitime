/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ssb.gestitime.databases;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.ssb.gestitime.R;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * gestitime
 * 15/06/2016. Sergio Sánchez Barahona. sesaba23@gmail.com
 * It creates or updates the database application in order to save timers
 */
public class CgestitimeDataBaseHelper extends SQLiteOpenHelper {

    private ResultSet cdr; // Reference to cursor to store queries
    private ProgressDialog progressDialog;
    private Context context;
    /**
     * DATABASE VERSION
     */
    private static final int DB_VERSION = 1;

    /**
     * TABLE STRINGS
     */
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String DATE_TYPE = " DATETIME";
    private static final String COMMA = ", ";

    /**
     * SQL CREATE RECORD_TIMERS TABLE sentence
     */
    private static final String CREATE_RECORD_TIMERS_TABLE =
              "CREATE TABLE " + DatabaseContract.RecordTimers.TABLE_NAME + "("
            + DatabaseContract.RecordTimers._ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT, "
            + DatabaseContract.RecordTimers.NAME_RECORD + TEXT_TYPE + COMMA
            + DatabaseContract.RecordTimers.DESCRIPTION_RECORD + TEXT_TYPE + COMMA
            + DatabaseContract.RecordTimers.ID_USER + INTEGER_TYPE + ");";


    /**
     * SQL CREATE LIST_RECORD_TIMERS sentence
     */
    private static final String CREATE_LIST_RECORD_TIMERS_TABLE =
            "CREATE TABLE " + DatabaseContract.ListRecordTimers.TABLE_NAME + "("
                    + DatabaseContract.ListRecordTimers._ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT, "
                    + DatabaseContract.ListRecordTimers.ID_RECORD + INTEGER_TYPE + COMMA
                    + DatabaseContract.ListRecordTimers.TIME_ITEM + TEXT_TYPE + ");";

    //Constructor
    public CgestitimeDataBaseHelper(Context context){
        super(context, DatabaseContract.DB_NAME, null, DB_VERSION); //Null is an advanced feature relating cursors
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        /* Creates RECORD TIMERS table */
        db.execSQL(CREATE_RECORD_TIMERS_TABLE);
        /* Creates estate table */
        db.execSQL(CREATE_LIST_RECORD_TIMERS_TABLE);
        // Uncomment in case you want to Make work in background thread
//        new ConectionMySQLTask().execute(db);
        prepopulateDataBase(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }

    /* This method helps to introduce data registers in the data Base's RECORD_TIMERS table*/
    private static void insertRecordTimer(SQLiteDatabase db, String nameRecord, String descriptionRecord,
                                          int idUser){
        /*Helps to insert date format in "uwfact" field*/
//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        //Object to create pairs of name-values
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.RecordTimers.NAME_RECORD, nameRecord);
        values.put(DatabaseContract.RecordTimers.DESCRIPTION_RECORD, descriptionRecord);
        values.put(DatabaseContract.RecordTimers.ID_USER, idUser);

        db.insert(DatabaseContract.RecordTimers.TABLE_NAME, null, values);
    }

    /* This method helps to introduce data registers in the data Base's LIST_RECORD_TIMERS table*/
    private static void insertListRecordTimer(SQLiteDatabase db, int idRecord, long timeItem){
        //Object to create pairs of name-values
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.ListRecordTimers.ID_RECORD, idRecord);
        values.put(DatabaseContract.ListRecordTimers.TIME_ITEM, timeItem);

        db.insert(DatabaseContract.ListRecordTimers.TABLE_NAME, null, values);
    }

    /**
     * Prepopulate the data base with some examples
     */
    private void prepopulateDataBase(SQLiteDatabase... db){

        Resources r = context.getResources();
        /*Insert one record in the dataBase as an example.
              In the first version of the dataBase it is not necesary and ID_USER as it is not implemented yet*/
        insertRecordTimer(db[0], r.getString(R.string.break_title_default_db), r.getString(R.string.break_description_default_db), 1);
        insertRecordTimer(db[0], r.getString(R.string.lunch_title_default_db), r.getString(R.string.lunch_description_default_db), 2);
        insertRecordTimer(db[0], r.getString(R.string.paella_title_default_db), r.getString(R.string.paella_description_default_db), 3);
        insertRecordTimer(db[0], r.getString(R.string.fideua_title_default_db), r.getString(R.string.fideua_description_default_db), 4);
        insertRecordTimer(db[0], r.getString(R.string.exam_title_default_db), r.getString(R.string.exam_description_default_db), 5);
        insertRecordTimer(db[0], r.getString(R.string.bottle_title_default_db), r.getString(R.string.bottle_description_default_db), 6);
        insertRecordTimer(db[0], r.getString(R.string.meeting_title_default_db), r.getString(R.string.meeting_description_default_db), 7);
        Log.d("CDataBaseHelper", "Registro de Prueba creado en la tabla RECORD_TIMERS");
        insertListRecordTimer(db[0], 1, 1500); //Insert 25 minutes notification
        insertListRecordTimer(db[0], 1, 1680); //Insert 28 minutes notification
        insertListRecordTimer(db[0], 1, 1800); //Insert 30 minutes notification
        insertListRecordTimer(db[0], 2, 2700); //Insert 45 minutes notification
        insertListRecordTimer(db[0], 2, 4800); //Insert 80 minutes notification
        insertListRecordTimer(db[0], 2, 5400); //Insert 90 minutes notification
        insertListRecordTimer(db[0], 3, 300); //Insert 5 minutes notification
        insertListRecordTimer(db[0], 3, 600); //Insert 10 minutes notification
        insertListRecordTimer(db[0], 3, 900); //Insert 15 minutes notification
        insertListRecordTimer(db[0], 3, 1200); //Insert 20 minutes notification
        insertListRecordTimer(db[0], 4, 150); //Insert 5 minutes notification
        insertListRecordTimer(db[0], 4, 300); //Insert 10 minutes notification
        insertListRecordTimer(db[0], 4, 450); //Insert 15 minutes notification
        insertListRecordTimer(db[0], 4, 600); //Insert 20 minutes notification
        insertListRecordTimer(db[0], 5, 1200); //Insert 20 minutes notification
        insertListRecordTimer(db[0], 5, 2400); //Insert 40 minutes notification
        insertListRecordTimer(db[0], 5, 3300); //Insert 55 minutes notification
        insertListRecordTimer(db[0], 5, 3600); //Insert 60 minutes notification
        insertListRecordTimer(db[0], 6, 10800); //Insert 3 hours notification
        insertListRecordTimer(db[0], 6, 21600); //Insert 6 hours notification
        insertListRecordTimer(db[0], 6, 32400); //Insert 9 hours notification
        insertListRecordTimer(db[0], 6, 43200); //Insert 12 hours notification
        insertListRecordTimer(db[0], 6, 54000); //Insert 15 hours notification
        insertListRecordTimer(db[0], 6, 64800); //Insert 18 hours notification
        insertListRecordTimer(db[0], 6, 75600); //Insert 21 hours notification
        insertListRecordTimer(db[0], 6, 86400); //Insert 24 hours notification
        insertListRecordTimer(db[0], 7, 300); //Insert 5 minutes notification
        insertListRecordTimer(db[0], 7, 600); //Insert 10 minutes notification
        insertListRecordTimer(db[0], 7, 900); //Insert 15 minutes notification
        Log.d("CDataBaseHelper", "Registros de tiempo creado en la tabla LIST_RECORD_TIMERS");

    }

    /**
     * In case we want to prepopulate the dataBase with asynchronous task in the future
     */
    private class ConectionMySQLTask extends AsyncTask<SQLiteDatabase, Void, Void> {
        @Override
        protected Void doInBackground(SQLiteDatabase... db){
            return null;
        }
        @Override
        protected void onPreExecute() {
            /* With the last two arguments the progress bar is indeterminate and you can not cancel the execution*/
//            progressDialog = ProgressDialog.show(context,
//                    context.getResources().getString(R.string.title_downloading_database_dialog),
//                    context.getResources().getString(R.string.message_update_database_dialog),
//                    true, false);
        }
        @Override
        protected void onPostExecute(Void v) {
//            progressDialog.dismiss();
//            Toast.makeText(context, R.string.finish_downloading_database, Toast.LENGTH_LONG).show();
        }
    }
}
