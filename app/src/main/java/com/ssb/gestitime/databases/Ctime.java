/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ssb.gestitime.databases;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * getitime
 * Created by sergio on 18/05/16.
 * Stores times and provide method to get and set them.
 * It also implements a Parcelable in order to putExtra data through intents
 */
public class Ctime implements Parcelable {

    private int index; //Number key identifier
    private long timeSeconds;
    private String timeString; //Time as a string format (hh:mm:ss)
    private boolean completed; //Indicates if this time has been completed
    private long timeLeft; //Store time left to current interval.

    //Class implements parcelable in order to communicate between activities using putExtra
    public static final Parcelable.Creator<Ctime> CREATOR = new Parcelable.Creator<Ctime>(){

        @Override
        public Ctime createFromParcel(Parcel source) {
            return new Ctime(source);
        }
        @Override
        public Ctime[] newArray(int size) {
            return new Ctime[size];
        }
    };

    public Ctime(long timeSeconds, long timeLeft, String timeString) {
        setTimeSeconds(timeSeconds);
        setTimeLeft(timeLeft);
        setTimeString(timeString);
        completed = false;
    }

    public Ctime(Parcel in){
        setTimeSeconds(timeSeconds);
        setTimeString(timeString);
        completed = false;
        readFromParcel(in);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public long getTimeSeconds() {
        return timeSeconds;
    }

    public void setTimeSeconds(long timeSeconds) {
        this.timeSeconds = timeSeconds;
    }

    public String getTimeString() {
        return timeString;
    }

    public void setTimeString(String timeString) {
        this.timeString = timeString;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public long getTimeLeft() {return timeLeft; }

    public void setTimeLeft(long timeLeft) { this.timeLeft = timeLeft; }

    //Override methods to implement parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(timeSeconds);
        dest.writeString(timeString);
        //There is no readBoolean method in parcelables
        dest.writeBooleanArray(new boolean[]{completed});
    }

    private void readFromParcel(Parcel in){
        timeSeconds = in.readLong();
        timeString = in.readString();
        //There is no readBoolean method in parcelables
        boolean[] temp = new boolean[1];
        in.readBooleanArray(temp);
        completed = temp[0];
    }


}
