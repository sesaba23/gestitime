/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ssb.gestitime.pickers;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;

import com.ssb.gestitime.R;

import java.lang.reflect.Field;

/**
 * gestitime
 * Created by sergio on 15/05/16.
 *
 * Customize number pickers according my needs
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB) //For Backward-compability
public class MyNumberPicker extends NumberPicker {

    public MyNumberPicker(Context context) {
        super(context);
    }

    public MyNumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        processAttributeSet(attrs);
    }

    public MyNumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        processAttributeSet(attrs);

    }

    /**
     * //This method reads the parameters given in the xml file and sets the properties according to it
     * @param attrs
     */
    private void processAttributeSet(AttributeSet attrs) {
        this.setMinValue(attrs.getAttributeIntValue(null, "set_min_value", 0));
        this.setMaxValue(attrs.getAttributeIntValue(null, "set_max_value", 59)); //What to return if the attribute isn't found in the layout xml

        //Format the view always with two digits
        this.setFormatter(new Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d", value);
            }
        });

        this.setDividerColor(this, attrs.getAttributeIntValue(null, "divider_color", getResources().getColor(android.R.color.transparent)));

        //Focus is not allow on the number picker, so the keyboard doesn't even appear
        this.setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);

//        this.setWrapSelectorWheel(true);

//        this.setNumberPickerText(this, getResources().getColor(R.color.colorPrimary));

    }

    /**
     * This method change the color or the selection divider's number picker
     * @param picker
     * @param color
     */
    private void setDividerColor(NumberPicker picker, int color) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    /**
     * This method change text propierties of the number picker
     * @param numberPicker
     * @param color
     * @return
     */
    private boolean setNumberPickerText(NumberPicker numberPicker, int color)
    {
        final int count = numberPicker.getChildCount();
        for(int i = 0; i < count; i++){
            View child = numberPicker.getChildAt(i);
            if(child instanceof EditText){
                try{
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint)selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText)child).setTextColor(color);
                    ((EditText)child).setTextSize(50);
                    numberPicker.invalidate();
                    return true;
                }
                catch(NoSuchFieldException e){
                    Log.w("setNumPickerTextColor", e);
                }
                catch(IllegalAccessException e){
                    Log.w("setNumPickerTextColor", e);
                }
                catch(IllegalArgumentException e){
                    Log.w("setNumPickerTextColor", e);
                }
            }
        }
        return false;
    }

    @Override
    public void addView(View child) {
        super.addView(child);
        updateView(child, 30);
    }

    @Override
    public void addView(View child, int index, android.view.ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        updateView(child, 30);
    }

    @Override
    public void addView(View child, android.view.ViewGroup.LayoutParams params) {
        super.addView(child, params);
        updateView(child, 30);
    }

    private void updateView(View view, int textSize) {
        if(view instanceof EditText){
            ((EditText) view).setTextSize(textSize);
//            ((EditText) view).setTextColor(getResources().getColor(R.color.colorPrimary));
        }
    }



} //End of class
