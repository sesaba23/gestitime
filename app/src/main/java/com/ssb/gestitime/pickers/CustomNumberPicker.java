/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.ssb.gestitime.pickers;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.widget.NumberPicker;
import com.ssb.gestitime.R;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * CustomCountDownViewsTry Created by Sergio Sánchez Barahona on 2/08/16.
 */
public class CustomNumberPicker extends NumberPicker {

    private Context mContext;
    public CustomNumberPicker(Context context) {
        super(context);
        this.mContext = context;
    }

    public CustomNumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        processAttributeSet(attrs);
    }

    public CustomNumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        processAttributeSet(attrs);
    }

    /**
     * //This method reads the parameters given in the xml file and sets the properties according to it
     * @param attrs
     */
    private void processAttributeSet(AttributeSet attrs) {
        this.setMinValue(attrs.getAttributeIntValue(null, "set_min_value", 0));
        this.setMaxValue(attrs.getAttributeIntValue(null, "set_max_value", 59)); //What to return if the attribute isn't found in the layout xml

        //Format the view always with two digits
        this.setFormatter(new Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d", value);
            }
        });

        this.setDividerColor(this, attrs.getAttributeIntValue(null, "divider_color", getResources().getColor(R.color.colorPrimaryDark)));

        //Focus is not allow on the number picker, so the keyboard doesn't even appear
        this.setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);

    }




    /**
     * using reflection to change the value because
     * changeValueByOne is a private function and setValue
     * doesn't call the onValueChange listener.
     *
     * @param increment
     *            the increment. true: to increment and false to decrement
     */
    public void changeValueByOne(final boolean increment) {

        Method method;
        try {
            // reflection call for
            method = getClass().getSuperclass().getDeclaredMethod("changeValueByOne", boolean.class);
            method.setAccessible(true);
            method.invoke(this, increment);

        } catch (final NoSuchMethodException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method change the color or the selection divider's number picker
     * @param picker
     * @param color
     */
    public void setDividerColor(NumberPicker picker, int color) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }//End of Class



}
