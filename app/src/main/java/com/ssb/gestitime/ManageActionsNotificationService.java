/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ssb.gestitime;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.ssb.gestitime.fragment.MainFragment;
import com.ssb.gestitime.utils.Constants;

/**
 * ManageActionsNotificationService.java
 * GestiTime Created by Sergio Sánchez Barahona on 29/07/16.
 * This Service helps to communicate with MainFragment
 * in order to control contdown with notification's action buttons.
 */
public class ManageActionsNotificationService extends Service {

    /**
     * The MainFragment must to implement this interface so whe can update countdown state
     */
    public interface CallBacks{
        void updateState(String action);
    }

    private CallBacks callBack;
    private final IBinder binder;

    /**
     * Binder implementation overriding its onBind method so MainFragment can
     * binds with this service in order to access service methods
     */
    public class NotificationBinder extends Binder {

        public ManageActionsNotificationService getTimerService() {
            return ManageActionsNotificationService.this;
        }
    }
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }


    public ManageActionsNotificationService() {
        super();
        //Create a binder to join service with fragment
        binder = new NotificationBinder();
    }

    /**
     * Helps to register MainFragment as an observer so it gets update
     * @param mainFragment
     */
    public void registerClient(MainFragment mainFragment){
        this.callBack = mainFragment;
    }

    /**
     * Execute in the main threat when the service is started. Select what action
     * has been clicked in the notification
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        String action = intent.getAction();
        switch (action){
            case Constants.ACTION.PAUSE_ACTION:
                callBack.updateState(Constants.ACTION.PAUSE_ACTION);
                break;
            case Constants.ACTION.PLAY_ACTION:
                callBack.updateState(Constants.ACTION.PLAY_ACTION);
                break;
            case Constants.ACTION.STOP_ACTION:
                callBack.updateState(Constants.ACTION.STOP_ACTION);
                break;
            default:
                break;
        }
        stopService(intent);
        return START_STICKY;
    }
}
