/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ssb.gestitime;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ScrollView;
import android.widget.Toast;

import com.ssb.gestitime.inscription.ChangeLogDialog;
import com.ssb.gestitime.inscription.WhatsNewDialog;
import com.ssb.gestitime.databases.Ctime;
import com.ssb.gestitime.fragment.MainFragment;
import com.ssb.gestitime.fragment.RecordTimersDialog;
import com.ssb.gestitime.fragment.SaveTimesListFragment;
import com.ssb.gestitime.utils.Constants;
import com.ssb.gestitime.utils.DeviceType;
import com.ssb.gestitime.utils.MyLogger;
import com.ssb.gestitime.utils.TimeUtility;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * getitime
 * 20/05/5016. Sergio Sánchez Barahona
 * The main activity of the application
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        SaveTimesListFragment.OnRecordSelectedListener,
        RecordTimersDialog.RecordDialogListener{
    private Menu menu;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private long doubleBackToExitPressedOnce;
    private static final int TIME_INTERVAL = 2000; //ms, desired time passed between two back presses.
    private Toast toast = null; //To show message on one back button pressed
    //Reference to control notification preferences
    private SharedPreferences preferences;

    //Attach custom font by default in this activity
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyLogger.d("ChangeConfig", "Creating view of Activity");

        if(savedInstanceState == null) {
            MyLogger.d("ChangeConfig", "SaveInstantState is null, replacing fragment");
            MainFragment mainFragment = new MainFragment();
            FragmentTransaction ft1 = getFragmentManager().beginTransaction();
            ft1.replace(R.id.content_main_fragment, mainFragment)
//                .addToBackStack(LAYOUT_INFLATER_SERVICE)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();

            SaveTimesListFragment saveTimesListFragment = new SaveTimesListFragment();
            FragmentTransaction ft2 = getFragmentManager().beginTransaction();
            ft2.replace(R.id.save_records_listview, saveTimesListFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();

            //Configure toolBar
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            toolbar.setLogo(R.mipmap.ic_notification);

        /* TODO Uncomment if in the future is necessarily to set floating action buttons or navigation drawer*/

            fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);

            //Launch what's new dialog (will only be shown once)
            final WhatsNewDialog whatsNewDialog = new WhatsNewDialog(this);
            whatsNewDialog.show();

            //Get user preferences
            preferences = PreferenceManager.getDefaultSharedPreferences(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Get Notification Preferences in order to set if screen turn off while on main screen
        final boolean holdScreenOnFlag = preferences.getBoolean("hold_on_screen", false);
        if(holdScreenOnFlag){
            //Hold the screen on
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }else{
            //Turn off screen according device settings
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }


         /* When we are running in a phone screen orientation is always portrait */
        if(!DeviceType.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            /*If we press back button when the countdown is running, back to main screen and stop counter
            * if we press back button when we are in configuration screen, exit the application*/
            ScrollView countdownView = (ScrollView)findViewById(R.id.layout_time_view);
            if(countdownView.isShown()) {
                MainFragment fragment = (MainFragment) getFragmentManager().findFragmentById(R.id.content_main_fragment);
                fragment.stopClicked();
                fragment.setStopView();
            }else {
                //If we want to to exit the application remove notification
                NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                notificationManager.cancel(TimerService.NOTIFICATION_ID);

                if(doubleBackToExitPressedOnce + TIME_INTERVAL > System.currentTimeMillis()) {
                    toast.cancel();
                    super.onBackPressed();
                    overridePendingTransition(R.anim.open_next, R.anim.close_main);
                    return;
                }else {
                    toast = Toast.makeText(this, R.string.text_on_back_pressed_to_exit, Toast.LENGTH_SHORT);
                    toast.show();
                }
                doubleBackToExitPressedOnce = System.currentTimeMillis();

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        this.menu = menu;
        return true;
    }

    /**
     * Configure actionBar buttons
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.open_next, R.anim.close_main);
            return true;
        }
        if(id == R.id.action_help){
             /* Open about Activity*/
            Intent intent = new Intent(this, HelpActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.open_next, R.anim.close_main);
        }
        if(id == R.id.action_about){
             /* Open about Activity*/
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.open_next_down, R.anim.close_main);
        }
        if(id == R.id.action_translation){
            Intent intent = new Intent(this, TranslationActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.open_next, R.anim.close_main);
        }
        if(id == R.id.action_full_screen){
            MainFragment fragment = (MainFragment)getFragmentManager().findFragmentById(R.id.content_main_fragment);

            Intent intent = new Intent(MainActivity.this, CountDownFullscreenActivity.class);
            //First of all, we need to show the actual time even if it is paused (In this state timerService CallBacks are not called to update time)
            intent.putExtra(Constants.ACTION.SECONDS, fragment.getSeconds());
            //Before we set full screen layout we need to know if the countdown is pause or running
            if(fragment.isCountDownPaused()){
                intent.setAction(Constants.ACTION.PAUSE_ACTION);
            }else {
                intent.setAction(Constants.ACTION.PLAY_ACTION);
            }
            startActivity(intent);

        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String subject = getString(R.string.app_name) + " - " + getString(R.string.text_about_body);
            String shareBody = "https://play.google.com/store/apps/details?id=" + getString(R.string.app_pname);
            String sendVia = getString(R.string.text_send_via);
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, sendVia));
            overridePendingTransition(R.anim.open_next, R.anim.close_main);
        } else if (id == R.id.nav_rate) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getString(R.string.app_pname))));
            overridePendingTransition(R.anim.open_next, R.anim.close_main);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public NavigationView getNavigationView(){
//        return (NavigationView) findViewById(R.id.nav_view);
        return null;
    }


    //Interface to communicate with the Drawer Fragment "SaveTimesListFragment
    @Override
    public void onRecordSelected(ArrayList<Ctime> CtimesArray, String record) {

//        fab.show();
//        YoYo.with(Techniques.FadeOut).delay(0).duration(0).playOn(fab);
//        YoYo.with(Techniques.BounceInUp).delay(500).duration(2000).playOn(fab);
        MyLogger.d("SaveTimesFragment", "Communication with Drawer Fragment has been correct");

        toolbar.setTitle(record);

        MainFragment mainFragment = (MainFragment)
                getFragmentManager().findFragmentById(R.id.content_main_fragment);

        if(mainFragment != null){
            mainFragment.setCTimesArray(CtimesArray);
        }
    }

    /**
     * Interface to communicate with the Dialog Fragment in order to save timers
     *
     */
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

        MainFragment mainFragment = (MainFragment)
                getFragmentManager().findFragmentById(R.id.content_main_fragment);
        MyLogger.d("SaveDialog", "Entro en aceptar");

        if(dialog != null) {
            MyLogger.d("SaveDialog", "llamo al mainFragment");
            RecordTimersDialog recordTimersDialog = (RecordTimersDialog) dialog;

            mainFragment.saveListviewInDatabase(recordTimersDialog.getTitle(), recordTimersDialog.getDescription());
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }
}//End of class
