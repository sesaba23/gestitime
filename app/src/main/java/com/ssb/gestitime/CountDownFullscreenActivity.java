/*
 * Copyright 2016 (C) Sergio Sánchez Barahona
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.ssb.gestitime;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;
import android.view.WindowManager;
import android.widget.TextView;

import com.ssb.gestitime.databases.Ctime;
import com.ssb.gestitime.utils.Constants;
import com.ssb.gestitime.utils.MyLogger;
import com.ssb.gestitime.utils.TimeUtility;
import com.ssb.myflipclock.Flipmeter;

import java.util.ArrayList;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class CountDownFullscreenActivity extends AppCompatActivity implements TimerService.CallBacks {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private Flipmeter flipCountDownView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            flipCountDownView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                //I think this is a more elegant way to show flip clock countdown. User can press back button
//                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    /* Variables to manage/connect to the app services implementing countdown and notifications action buttons*/
    private Intent timerIntent; //References to an timerIntent so whe can launch the service
    private TimerService timer; //Reference to function timer
    private ServiceConnection timerConnection;

    public CountDownFullscreenActivity() {

        /*Initialize service timerConnection*/
        timerConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                TimerService.TimerBinder timerBinder = (TimerService.TimerBinder) service;
                timer = timerBinder.getTimerService();
                timer.registerClient(CountDownFullscreenActivity.this); //Set callback in this class to update seconds
            }
            @Override
            public void onServiceDisconnected(ComponentName name){
                MyLogger.d("HowServicesWork", "Unbind fullscreen of time service");
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_count_down_fullscreen);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mVisible = true;

        //Set textViews to show information about countdown clock
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        flipCountDownView = (Flipmeter)findViewById(R.id.flipmeter_time_view);

        // Set up the user interaction to manually show or hide the system UI.
        flipCountDownView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        findViewById(R.id.fullscreen_button1).setOnTouchListener(mDelayHideTouchListener);

        /* Start Services */
        timerIntent = new Intent(this, TimerService.class);
        bindService(timerIntent, timerConnection, Context.BIND_AUTO_CREATE); //Creates the Timer service if it doesn't already exist
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Hold the screen on while we are on fullscreen
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        TextView button = (TextView) findViewById(R.id.fullscreen_button1);
        // Change button1 text according to countdown state (running or paused)
        if(getIntent().getAction().equalsIgnoreCase(Constants.ACTION.PLAY_ACTION)){
            button.setText(getString(R.string.pause_button_text));
        }else {
            button.setText(getString(R.string.resume_button_text));

            long seconds = getIntent().getLongExtra(Constants.ACTION.SECONDS, 0);
            //When layout is shown, first of all, set actual time even in pause state (timerService Callbacks are not called in this state)
            String timeWithoutColons = TimeUtility.convertSecondsInTimeWithoutColons(seconds);
            flipCountDownView.setValue(Integer.parseInt(timeWithoutColons.trim()), true);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbindService(timerConnection);
        finish();
    }

    /**
     * When we pressed button1 we need to pause or resume countdown. This action is send to a service
     * in order to comumnicate with MainFragment which controls countdown
     * @param v
     */
    public void fullScreenButton1Cliked(View v){
        TextView button = (TextView) v.findViewById(R.id.fullscreen_button1);
        Intent intent = new Intent(this, ManageActionsNotificationService.class);
        if(button.getText().equals(getString(R.string.pause_button_text))){
            button.setText(getString(R.string.resume_button_text));
            button.setTextColor(getResources().getColor(R.color.materialGreen900));
            intent.setAction(Constants.ACTION.PAUSE_ACTION);
        }else{
            button.setText(getString(R.string.pause_button_text));
            button.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            intent.setAction(Constants.ACTION.PLAY_ACTION);
        }
        startService(intent);
    }

    /**
     * When we pressed button2 we need to stop countdown. This action is send to a service
     * in order to communicate with MainFragment which controls countdown.
     * Then come back to main screen
     * @param v
     */
    public void fullScreenButton2Cliked(View v){
        Intent intent = new Intent(this, ManageActionsNotificationService.class);
        intent.setAction(Constants.ACTION.STOP_ACTION);
        startService(intent);
        unbindService(timerConnection);
        finish();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        flipCountDownView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    @Override
    public void updateClient(long seconds, int i) {

        if(seconds >= 0) {
            //Show countdown
           String timeWithoutColons = TimeUtility.convertSecondsInTimeWithoutColons(seconds);
           flipCountDownView.setValue(Integer.parseInt(timeWithoutColons.trim()), true);
            //When countdown finishes enable buttons and stop service
            if(seconds == 0){
                //Show only the countdown
                timerIntent.putExtra(TimerService.NUM_NOTIFICATIONS, 0);
                stopService(timerIntent);
                finish();
            }
        }
    }
}
